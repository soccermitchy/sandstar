
install(FILES sandstar_service/sandstar.service DESTINATION etc/init)
install(FILES opt/skyspark/var/security/AllowedCorsUrl.config DESTINATION etc/)
ADD_CUSTOM_TARGET(copyAllowedCorsUrlConfig ALL COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/etc/ COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_CURRENT_LIST_DIR}/../opt/skyspark/var/security/AllowedCorsUrl.config ${CMAKE_BINARY_DIR}/etc/ COMMENT "copying AllowedCorsUrl.config")
install(DIRECTORY ${CMAKE_SOURCE_DIR}/usr/local/config DESTINATION etc/)
ADD_CUSTOM_TARGET(copyEngineTablesDatabase ALL COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/etc/ COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_LIST_DIR}/../usr/local/config/ ${CMAKE_BINARY_DIR}/etc/config COMMENT "copying engine tables database")

install(FILES EacIo/apps/platUnix.sax DESTINATION share)



set(CPACK_GENERATOR "DEB" CACHE STRING "" FORCE)
set(CPACK_PACKAGE_CONTACT "info@ankalabs.com" CACHE STRING "" FORCE)
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Anka Labs Team" CACHE STRING "" FORCE)
set(CPACK_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX} CACHE STRING "" FORCE)
set(CPACK_SET_DESTDIR ON CACHE BOOL "" FORCE)
set(CPACK_PACKAGE_VERSION_MAJOR ${sandstar_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${sandstar_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${sandstar_VERSION_PATCH})
SET(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_SOURCE_DIR}/deb/postinst;${CMAKE_SOURCE_DIR}/deb/postrm;${CMAKE_SOURCE_DIR}/deb/prerm")

# message(${CMAKE_SYSTEM_NAME})
set(CPACK_PACKAGE_FILE_NAME "sandstar-${sandstar_VERSION}-${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}-${GIT_BRANCH}-${GIT_COMMIT_HASH}")

if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_FILE_NAME}-debug")
endif(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
include(CPack)
