//
// File: BoolOut.sedona
//
// Description:
//  
// History:
//   17 May 2017  Kushal Dalsania    Baseline
//

**
** boolOutPoint
**   Defines native functions to interact with digital outputs
**

class boolOutPoint
{
  static native bool set(int channel, bool bStatus);
}

**
** BoolOutput
**   Defines Digital Output component
**

public class BoolOutput extends Component
{
  ////////////////////////////////////////////////////////////////
  // Properties
  ////////////////////////////////////////////////////////////////
  
  ** The digital output channel name.
  @readonly @asStr property Buf(50) channelName = ""
  
  ** The digital output channel.
  @readonly property int channel;
  
  ** Output
  @readonly property bool out = null;
  
  ** Input Trigger. Default value -> null
  @allowNull property bool in1 = null;
  @allowNull @summary=false property bool in2 = null;
  @allowNull @summary=false property bool in3 = null;
  @allowNull @summary=false property bool in4 = null;
  @allowNull @summary=false property bool in5 = null;
  @allowNull @summary=false property bool in6 = null;
  @allowNull @summary=false property bool in7 = null;
  @allowNull property bool in8 = null;
  @allowNull @summary=false property bool in9 = null;
  @allowNull property bool in10 = null;
  @allowNull @summary=false property bool in11 = null;
  @allowNull @summary=false property bool in12 = null;
  @allowNull @summary=false property bool in13 = null;
  @allowNull @summary=false property bool in14 = null;
  @allowNull @summary=false property bool in15 = null;
  @allowNull property bool in16 = null;
  property bool relinquishDefault = false;
  
  ** Marker List
  @config @asStr property Buf(100) pointQuery;
  
  ** Count.
  @readonly property int pointQuerySize;
  
  ** point query status. 
  @allowNull=false
  @trueText="ok" @falseText="fault"
  @readonly property bool pointQueryStatus;
  
  ** Temporary Marker List
  inline Str(100) tempPointQuery = "nan";
  
  ** Current Status on channel
  @readonly @asStr property Buf(20) curStatus = "na";

  ** Operating Status
  @allowNull=false
  @trueText="yes" @falseText="no"
  @readonly property bool enabled;
  
  ////////////////////////////////////////////////////////////////
  // Class Global
  ////////////////////////////////////////////////////////////////
  private long ticks;
  
  ////////////////////////////////////////////////////////////////
  // Action Methods
  ////////////////////////////////////////////////////////////////
  
  action void query()
  {           
    tempPointQuery.copyFromStr(pointQuery.toStr(), 100);
	
	// Count total records that contains similar pointQuery
	pointQuerySize := eacio.getRecordCount(pointQuery.toStr());
	
	pointQueryStatus := (pointQuerySize == 1)?(true):(false);
	
	// Get channel number
	channel := eacio.resolveChannel(pointQuery.toStr());
	
	if(channel != 0)
	{
	  //Enabled?
	  enabled := eacio.isChannelEnabled(channel);
	  
	  // Update sedonaId and sedonaType tag
	  eacio.writeSedonaId(channel, this.id);
	  eacio.writeSedonaType(channel, this.type.kit.name, this.name);
	
	  // Update channel name
	  // FIX: Updating Buf in native code do not update the Buf member 'size'.
	  // Below fix updates the size of Buf according to size of string.
	  
	  eacio.getChannelName(channel, channelName.toStr());
	  channelName.copyFromStr(channelName.toStr());
	  changed(BoolOutput.channelName);
	  
	  out := relinquishDefault;
	}
  }
  
  // ------Overides methods------
  
  virtual override void start()
  {
	ticks = Sys.ticks();
  }
  
  virtual override void execute() 
  { 
    // TODO: As per SedonaDev documentation, No guarantee is made whether the 
	// string (returned by toStr()) is actually null terminated.
	
    if (!tempPointQuery.equals(pointQuery.toStr()))
	{
		tempPointQuery.copyFromStr(pointQuery.toStr(), 100);
		
		// Count total records that contains similar pointQuery
		pointQuerySize := eacio.getRecordCount(pointQuery.toStr());
		
		pointQueryStatus := (pointQuerySize == 1)?(true):(false);
		
		// Get channel number
		channel := eacio.resolveChannel(pointQuery.toStr());
		
		if(channel != 0)
		{
		  //Enabled?
		  enabled := eacio.isChannelEnabled(channel);
			
		  // Update sedonaId and sedonaType tag
		  eacio.writeSedonaId(channel, this.id);
		  eacio.writeSedonaType(channel, this.type.kit.name, this.name);
		
		  // Update channel name
		  // FIX: Updating Buf in native code do not update the Buf member 'size'.
		  // Below fix updates the size of Buf according to size of string.
			
		  eacio.getChannelName(channel, channelName.toStr());
		  channelName.copyFromStr(channelName.toStr());
		  changed(BoolOutput.channelName);
		  
		  out := relinquishDefault;
		}
	}
	
	//Update curStatus every interval
	if (Sys.ticks() > (ticks + 2sec))
	{
	  // Update for next trigger
	  ticks = Sys.ticks();
	  
	  if(channel!=0)
	  {
	    // enabled slot
	    enabled := eacio.isChannelEnabled(channel);
		
	    // curStatus
	    eacio.getCurStatus(channel, curStatus.toStr());
		curStatus.copyFromStr(curStatus.toStr());
		changed(BoolOutput.curStatus);
		
		//Level
		int level=0;
	    bool inValue;
	    
	    level=eacio.getLevel(channel);
	    if(level!=0)
	    {
	      float value=eacio.getLevelValue(channel,level);
		  if(value==0.0)
		  {
		    inValue=false;
		  }
		  else if (value==1.0)
		  {
		    inValue=true;
		  }
		  else // >=2.0
		  {
		    inValue=null
		  }
		  
		  switch(level)
		  {
		    case 1:
		      in1:=inValue;
		  	break;
		    case 2:
		      in2:=inValue;
		  	break;
		    case 3:
		      in3:=inValue;
		  	break;
		    case 4:
		      in4:=inValue;
		  	break;
		    case 5:
		      in5:=inValue;
		  	break;
		    case 6:
		      in6:=inValue;
		  	break;
		    case 7:
		      in7:=inValue;
		  	break;
		    case 8:
		      in8:=inValue;
		  	break;
		    case 9:
		      in9:=inValue;
		  	break;
		    case 10:
		      in10:=inValue;
		  	break;
		    case 11:
		      in11:=inValue;
		  	break;
		    case 12:
		      in12:=inValue;
		  	break;
		    case 13:
		      in13:=inValue;
		  	break;
		    case 14:
		      in14:=inValue;
		  	break;
		    case 15:
		      in15:=inValue;
		  	break;
		    case 16:
		      in16:=inValue;
		  	break;
		    case 17:
		      relinquishDefault:=inValue;
		  	break;
		  }
		  
		  //This is fix. When any input is set to null changed function is not called. Why?
		  //TODO: FIx this.
		  if((in1 == null) && (in2 == null) && (in3 == null) && (in4 == null) && (in5 == null) && (in6 == null) && (in7 == null) && (in8 == null) && (in9 == null) && (in10 == null) && (in11 == null) && (in12 == null) && (in13 == null) && (in14 == null) && (in15 == null) && (in16 == null))
  		  {
		    out := relinquishDefault;
		    boolOutPoint.set(channel, relinquishDefault);
		  }
	    }
	  }
	}
  }
  
  override void setToDefault(Slot slot) 
  {
	super.setToDefault(slot);
	
	if(slot.name == "in1")
	{
	  in1 := null;
	}
	else if(slot.name == "in2")
	{
	  in2 := null;
	}
	else if(slot.name == "in3")
	{
	  in3 := null;
	}
	else if(slot.name == "in4")
	{
	  in4 := null;
	}
	else if(slot.name == "in5")
	{
	  in5 := null;
	}
	else if(slot.name == "in6")
	{
	  in6 := null;
	}
	else if(slot.name == "in7")
	{
	  in7 := null;
	}
	else if(slot.name == "in8")
	{
	  in8 := null;
	}
	else if(slot.name == "in9")
	{
	  in9 := null;
	}
	else if(slot.name == "in10")
	{
	  in10 := null;
	}
	else if(slot.name == "in11")
	{
	  in11 := null;
	}
	else if(slot.name == "in12")
	{
	  in12 := null;
	}
	else if(slot.name == "in13")
	{
	  in13 := null;
	}
	else if(slot.name == "in14")
	{
	  in14 := null;
	}
	else if(slot.name == "in15")
	{
	  in15 := null;
	}
	else if(slot.name == "in16")
	{
	  in16 := null;
	}
	
	// If all 'null' set relinquishDefault
	if((in1 == null) && (in2 == null) && (in3 == null) && (in4 == null) && (in5 == null) && (in6 == null) && (in7 == null) && (in8 == null) && (in9 == null) && (in10 == null) && (in11 == null) && (in12 == null) && (in13 == null) && (in14 == null) && (in15 == null) && (in16 == null))
    {
	  out := relinquishDefault;
	  boolOutPoint.set(channel, relinquishDefault);
    }
  }

  virtual override void changed(Slot slot)
  {
	super.changed(slot);
	
	if (channel != 0)
	{
	  if(slot.name == "out")
	  {
	    boolOutPoint.set(channel, out);
	  }
	  else if((in1 != null) && (enabled == true))
	  {
	    out := in1;
	  }
	  else if((in2 != null) && (enabled == true))
	  {
	    out := in2;
	  }
	  else if((in3 != null) && (enabled == true))
	  {
	    out := in3;
	  }
	  else if((in4 != null) && (enabled == true))
	  {
	    out := in4;
	  }
	  else if((in5 != null) && (enabled == true))
	  {
	    out := in5;
	  }
	  else if((in6 != null) && (enabled == true))
	  {
	    out := in6;
	  }
	  else if((in7 != null) && (enabled == true))
	  {
	    out := in7;
	  }
	  else if((in8 != null) && (enabled == true))
	  {
	    out := in8;
	  }
	  else if((in9 != null) && (enabled == true))
	  {
	    out := in9;
	  }
	  else if((in10 != null) && (enabled == true))
	  {
	    out := in10;
	  }
	  else if((in11 != null) && (enabled == true))
	  {
	    out := in11;
	  }
	  else if((in12 != null) && (enabled == true))
	  {
	    out := in12;
	  }
	  else if((in13 != null) && (enabled == true))
	  {
	    out := in13;
	  }
	  else if((in14 != null) && (enabled == true))
	  {
	    out := in14;
	  }
	  else if((in15 != null) && (enabled == true))
	  {
	    out := in15;
	  }
	  else if((in16 != null) && (enabled == true))
	  {
	    out := in16;
	  }
	  else if (slot.name == "relinquishDefault")
	  {
	    out := relinquishDefault;
	  }
	}
  }
}
