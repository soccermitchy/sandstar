#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/AbstractHTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/NumberFormatter.h"
#include "Poco/Exception.h"
#include "Poco/ThreadPool.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/URI.h"
#include "Poco/Util/Application.h"

#include <fstream>
#include <iostream>
#include <boost/algorithm/string.hpp>
//#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "points.hpp"
#include "op.hpp"
#include "logger.h"

//#include "Poco/Thread.h"

// Namespaces

using Poco::Timestamp;
using Poco::DateTimeFormatter;
using Poco::DateTimeFormat;
using Poco::ThreadPool;
using Poco::Net::StreamSocket;
using Poco::Net::ServerSocket;
using Poco::Net::SocketAddress;
using Poco::Net::TCPServer;
using Poco::Net::TCPServerConnectionFilter;
using Poco::Net::TCPServerConnection;
using Poco::Net::TCPServerConnectionFactory;
using Poco::Net::TCPServerConnectionFactoryImpl;
using Poco::Net::TCPServerParams;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServerParams;
using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::HelpFormatter;

// Sedona linkage

extern int willTerminate();

// Config Macros
#define LOCALHOST_ENABLE (0)   // 1 -> Enable  0 -> Disable

// Class definitions

// HaystackRequestHandler
//
// Created by request factory to handle all unauthenticated
// http requests made to haystack server

class HaystackRequestHandler:public HTTPRequestHandler
{
private:
	// Local data
	haystack::PointServer &m_server;

public:
	// Contruction

	HaystackRequestHandler(haystack::PointServer &server):m_server(server) {}

	// Overrides

	// handleRequest
	// Called to handle incomming request

	void handleRequest(HTTPServerRequest &request,HTTPServerResponse &response)
	{
		// get url path
		const std::string path=Poco::URI(request.getURI()).getPath();

		// output message
		// std::cout << "-- MESSAGE [sys::Haystack] " << request.getMethod() << " " << path << " from " << request.clientAddress().toString() << std::endl;
		LOG_INFO_HAYSTACKOPS_MSG("%s %s from %s",request.getMethod().c_str(),path.c_str(),request.clientAddress().toString().c_str());
		// no path?
		if(path=="" || path=="/")
		{
			// redirect to about
			response.redirect("/about");
			return;
		}
		
		// set response encoding
		response.setChunkedTransferEncoding(true);
		
		// get slash pos
		// size_t slash=path.find('/',1);
		// if(slash==path.npos) slash=path.size();
		
		// get op name
		// std::string name=path.substr(1,slash);
		
		std::string strOp;
	size_t slashPos = path.find_first_of('/',1);
	if(slashPos == std::string::npos)
		strOp = path.substr(1);
	else
		strOp = path.substr(1,slashPos-1);

	// size_t slashPos = path.find_first_of('/',1);
		// std::string strOp;
		// path.substr(1,slashPos);
		// std::string strOpRemain;
		// if(slashPos != std::string::npos)
			// strOpRemain = str.substr(slashPos);

		
		haystack::Op *op=(haystack::Op *) m_server.op(strOp,false);
		
		if(op==NULL)
		{
			// op not found
			// std::cout << "-- ERROR [sys::Haystack] op '" << name  << "' not valid" << std::endl;
			LOG_ERR_HAYSTACKOPS_MSG("op '%s' not valid",strOp.c_str());

			response.setStatusAndReason(Poco::Net::HTTPResponse::HTTP_NOT_FOUND);
			response.send();

			return;
		}
		
		// process op
		// std::cout<<__FILE__<<":"<<__LINE__<<std::endl;
		op->on_service(m_server,request,response);
	}
};

// AuthRequestHandler
//
// Created by request factory to handle all authenticated
// http requests made to haystack server through /auth

class AuthRequestHandler:public Poco::Net::AbstractHTTPRequestHandler
{
private:
	// Local data
	haystack::PointServer &m_server;

public:
	// Construction

	AuthRequestHandler(haystack::PointServer &server):m_server(server) {}

	//
	//

	void handleRequest(HTTPServerRequest &request,HTTPServerResponse &response)
	{
		AbstractHTTPRequestHandler::handleRequest(request,response);
	}

	//
	//

	void run()
	{
		// get url path
		std::string path=Poco::URI(request().getURI()).getPath();

		// output message
		// std::cout << "-- MESSAGE [sys::Haystack] auth " << request().getMethod() << " " << path << " from " << request().clientAddress().toString() << std::endl;
		LOG_DEBUG_HAYSTACKOPS_MSG("auth %s %s from %s",request().getMethod().c_str(),path.c_str(),request().clientAddress().toString().c_str());

		// no path?
		if(path=="/auth" || path=="/auth/")
		{
			// redirect to about
			response().redirect("/auth/about");
			return;
		}

		// set response encoding
		response().setChunkedTransferEncoding(true);

		// remove /auth from path
		path=path.substr(5);//path.find("/auth/")+6);

		// get slash pos
		size_t slash=path.find('/',1);
		if(slash==path.npos) slash=path.size();

		// get op name
		std::string name=path.substr(1,slash);
		haystack::Op *op=(haystack::Op *) m_server.op(name,false);

		if(op==NULL)
		{
			// op not found
			// std::cout << "-- ERROR [sys::Haystack] auth op '" << name  << "' not valid" << std::endl;
			LOG_ERR_HAYSTACKOPS_MSG("auth op '%s' not valid",name.c_str());

			response().setStatusAndReason(Poco::Net::HTTPResponse::HTTP_NOT_FOUND);
			response().send();

			return;
		}

		// process op
		op->on_service(m_server,request(),response());
	}

	//
	//

	bool authenticate()
	{
		// basic example
		if(request().hasCredentials())
		{
			//
			std::string scheme;
			std::string authinfo;

			request().getCredentials(scheme,authinfo);

			// TODO: implement this

			// u: demo
			// p: demo

			if(scheme=="Basic" && authinfo=="ZGVtbzpkZW1v")
			return true;
		}

		//
		response().requireAuthentication("/auth");

		return false;
	}
};

// HaystackRequestHandlerFactory
//
//

class HaystackRequestHandlerFactory:public HTTPRequestHandlerFactory
{
private:
	// Local data
	haystack::PointServer &m_server;

public:
	// Construction

	HaystackRequestHandlerFactory(haystack::PointServer &server):m_server(server) {}

	// Overrides

	// createRequestHandler
	// Called to create handler for http request

	HTTPRequestHandler *createRequestHandler(const HTTPServerRequest &request)
	{
		const std::string path=Poco::URI(request.getURI()).getPath();

		// authenticated request?
		if(boost::starts_with(path,"/auth"))
		return new AuthRequestHandler(m_server);

		return new HaystackRequestHandler(m_server);
	}
};

class RejectFilter: public TCPServerConnectionFilter
{
private:
	std::vector<std::string> m_allowIPList;
public:
	
	RejectFilter()
	{
		struct stat buffer;
		Poco::Util::Application &app = Poco::Util::Application::instance();
		
		const std::string allowedIpListFile = std::string(app.config().getString("application.configDir")+"/AllowedCorsUrl.config");
		
		if(stat(allowedIpListFile.c_str(), &buffer)==0)
		{
			std::ifstream allowedIpFile(allowedIpListFile);
			std::copy(std::istream_iterator<std::string>(allowedIpFile), 
			std::istream_iterator<std::string>(), 
			std::back_inserter(m_allowIPList));
		}
		else
		{
			m_allowIPList.push_back("*");
			// std::cout << "-- MESSAGE [sys::Haystack] " << EACIO_FILE_ALLOWED_IP << " file not found. Adding '*' for Access-Control-Allow-Origin" << std::endl;
			LOG_DEBUG_HAYSTACKOPS_MSG("%s file not found. Adding '*' for Access-Control-Allow-Origin",allowedIpListFile.c_str());
		}
	}
	
	bool accept(const StreamSocket& ss)
	{
		bool result = false;
		
		//std::cout << "-- MESSAGE [sys::Haystack] " << ss.peerAddress().host().toString() << std::endl;
		
		if(std::find(m_allowIPList.begin(), m_allowIPList.end(), "*") != (m_allowIPList.end()))
		{
			result = true;
		}
		else
		{
			try
			{
				std::string origin = ss.peerAddress().host().toString();
				
				if (!origin.empty())
				{
					//std::cout << "ORIGIN: " << origin;
					for (std::vector<std::string>::iterator it = m_allowIPList.begin(), end = m_allowIPList.end();
					it!=end;
					it++)
					{
						if(it->find(origin) != std::string::npos)
						{
							result = true;
							break;
						}
					}
				}
			}
			catch (const std::exception& e)
			{
				// std::cout << "-- ERROR [sys::Haystack] " << e.what();
				LOG_ERR_HAYSTACKOPS_MSG("%s",e.what());
			}	
		}
		
		return result;
	}
};

