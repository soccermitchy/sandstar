#include "val.hpp"
#include "token.hpp"
#include <stdint.h>
#include <sstream>
#include <stdexcept>

namespace haystack {
/**
 * Stream based tokenizer for Haystack formats such as Zinc and Filters
 */
class Tokenizer
{
public:
    Token::ptr_t tok;           ///< current token type
    Val::auto_ptr_t val; ///< token literal or identifier
    int line_num = 1;  ///< current line number

    Tokenizer(std::istream& is);
//    Tokenizer(const std::string& s);
    virtual ~Tokenizer(){}
//	static std::auto_ptr<Tokenizer> make(const std::string& s);
    Token::ptr_t next();

private:

    void consume();
    void consume(int expected);

    Token::ptr_t read_id();
    Token::ptr_t read_num();
    Token::ptr_t read_str();
    Token::ptr_t read_ref();
    Token::ptr_t read_uri();
    Token::ptr_t read_date();
    Token::ptr_t read_time();
    Token::ptr_t read_datetime();
    Token::ptr_t read_symbol();

    Token::ptr_t date(const std::string& s);
    Token::ptr_t time(std::string s, bool addSeconds);
    Token::ptr_t date_time(std::stringstream& s);

    Token::ptr_t number(const std::string& s, int unitIndex);
    int32_t read_esc_char();

    static bool is_id_start(int32_t);
    static bool is_id_part(int32_t);
    static bool is_tz(int32_t);
    static bool is_unit(int32_t);
    static bool is_hex(int32_t);

    // Read a single scalar value from the stream.
    Val::auto_ptr_t read_val();
    Val::auto_ptr_t read_num_val();
    Val::auto_ptr_t read_bin_val();
    Val::auto_ptr_t read_coord_val();
    Val::auto_ptr_t read_xstr_val(std::string type);
    Val::auto_ptr_t read_ref_val();
    Val::auto_ptr_t read_str_val();
    Val::auto_ptr_t read_uri_val();
    Val::auto_ptr_t read_word_val();
    Val::auto_ptr_t read_list_val();
	Val::auto_ptr_t read_dict_type_val();

    void skipCommentsSL();
    void skipCommentsML();

    std::istream& m_is; // underlying stream
//    std::auto_ptr<std::istream> m_local_is;
    int32_t m_cur = 0;      // current char
    int32_t m_peek = 0;     // next char
    static const int m_eof = -1;
};

class parse_error : public std::runtime_error {
public:
    parse_error(const std::string & s) : runtime_error(s) {}
};
}

