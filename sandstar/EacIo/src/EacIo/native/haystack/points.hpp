// Copyright (c) 2015, J2 Innovations
// Copyright (c) 2012 Brian Frank
// Licensed under the Academic Free License version 3.0
// History:
//   09 Sep 2014  Radu Racariu<radur@2inn.com> Ported to C++
//   06 Jun 2011  Brian Frank  Creation

// Includes

#pragma once

#include "server.hpp"
#include "op.hpp" //TODO: remove
#include <Poco/AtomicCounter.h>
#include <Poco/RWLock.h>
#include <Poco/Timer.h>
#include <deque>

#define INVALID_CHANNEL_ID  (0)

// File Paths
// #define EACIO_FILE_LOG           ("/home/eacio/sandstar/EacIo/debugLog.txt")
// #define EACIO_FILE_ALLOWED_IP    ("/opt/skyspark/var/security/AllowedCorsUrl.config")
// #define EACIO_FILE_DATABASE_ZINC ("/home/eacio/sandstar/EacIo/database.zinc")

extern "C" {
#include "engineio.h"
}

// Namespace

namespace haystack
{
	// Forward references

    class Dict;

	// PointHistory class
	// This class provides a storage mechanism for point history

	class PointHistory:boost::noncopyable
	{
	public:
        typedef boost::shared_ptr<PointHistory> shared_ptr;

	public:
		// Construction
		PointHistory(const std::string &id);

	public:
		// Methods
		void Store(const std::string &curStatus,const Val &curVal,const TimeZone &timeZone=TimeZone::DEFAULT,bool totalized=false);
		void Fetch(const DateTimeRange &range,std::vector<HisItem> &his);

	private:
		// Local data
		std::deque<HisItem> m_items;
	};

	// PointServer class
    // This class provides an impl of Server for Sedona and engine

    class PointServer:public Server
    {
    public:
        friend class PointWatch;
        friend class PointHistory;

        typedef boost::ptr_map<std::string,Dict> recs_t;

        typedef std::map<std::string,Watch::shared_ptr> watches_t;
        typedef std::map<std::string,PointHistory::shared_ptr> history_t;

        const_iterator begin() const;
        const_iterator end() const;

    public:
    	// Construction
        PointServer();

    public:
        // Ops
        const std::vector<const Op *> &ops();
        const Op *const op(const std::string &name,bool checked=true) const;

    public:
        // About
        const Dict &on_about() const;

    protected:
        // Read
        Dict::auto_ptr_t on_read_by_id(const Ref& id) const;

    protected:
        // Navigation
        Grid::auto_ptr_t on_nav(const std::string& nav_id) const;
        Dict::auto_ptr_t on_nav_read_by_uri(const Uri& uri) const;

    protected:
        // Watches
        Watch::shared_ptr on_watch_open(const std::string& dis);
        const std::vector<Watch::shared_ptr> on_watches();

        Watch::shared_ptr on_watch(const std::string& id);

    protected:
        // Point Write
        Grid::auto_ptr_t on_point_write_array(const Dict& rec);
        void on_point_write(const Dict& rec, int level, const Val& val, const std::string& who, const Num& dur);

    protected:
        // History
        std::vector<HisItem> on_his_read(const Dict &rec,const DateTimeRange &range);
        void on_his_write(const Dict &rec,const std::vector<HisItem> &items);

    protected:
        // Actions
        Grid::auto_ptr_t on_invoke_action(const Dict& rec, const std::string& action, const Dict& args);

    public:
    	// Sedona linkage
		static int readCurVal(const int channel, double *curVal);
		static void writeCurVal(const int channel, double value);

		//static void writeVirtualVal(const int channel, double value);
		
		static void update_point(const Grid &g);
		static void add_point(const Grid &g);
		static void delete_point(const Grid &g);
		static t_ErrOp optimize_grid(void);
		static void commit_zinc(void);

		static int resolveMarkerList(char *marker);
		static int getRecordCount(char * markerList);

		static int getCurStatus(const int channel, char *curStatus);
		static int getChannelName(const int channel, char * channelName);
		
		static void writeComponentId (const int channel, double id);
		static void writeComponentType (const int channel, char* kit, char* name);
		
		static bool isChannelEnabled (const int channel);
		static bool getBoolTagValue (int channel, char* tag);
		static float getNumberTagValue(int channel, char* tag);
		static void getStringTagValue(int channel, char * tag, char * val);
		
		static char getTagType(int channel, char* tag);
		
		static int getWriteLevels(int channel);
		static double getwritelevelsvalue(int channel, int level);
		
	public:
		// Engine linkage
		static int engineTrigger(ENGINE_CHANNEL channel,ENGINE_VALUE *value);
		static int engineWriteAck(ENGINE_CHANNEL channel,ENGINE_VALUE *value);

	private:
		// Triggers
		static int triggerIPReset();

	private:
		// Point functions
        void add_point_custom(Grid &g);

	private:
		// History functions
		PointHistory::shared_ptr his_find(const std::string &id);
		void his_store(Dict *row,const std::string &type);

	private:
		// Timer functions
        void on_timer(Poco::Timer &timer);

		void timer_update();
		void timer_watches();
		void timer_history();

    public:
    	// Public data
        static std::vector<const Op *> *m_ops;
        static Dict *m_about;

		static recs_t m_recs;
		static std::vector<std::string> m_allowIPs;

    private:
    	// Private data
        watches_t m_watches;
        history_t m_history;

        Poco::Timer m_timer;
    };

	// PointWatch class
	// This class provides a storage mechanism for watch lists

	class PointWatch:public Watch
	{
	public:
		// Construction
		PointWatch(const PointServer &server,const std::string &dis);

	public:
		// Property access
		const std::string id() const;
		const std::string dis() const;

		const int lease() const;
		void lease(int);

	public:
		// Methods
		Grid::auto_ptr_t sub(const refs_t &ids,bool checked=true);
		void unsub(const refs_t &ids);

		Grid::auto_ptr_t poll_changes();
		Grid::auto_ptr_t poll_refresh();

		void close();
		bool is_open() const;

	private:
		// Local functions
		Grid::auto_ptr_t poll_check(bool refresh);

	private:
		// Local data
		const PointServer &m_server;

		const std::string m_uuid;
		const std::string m_dis;
		Poco::AtomicCounter m_lease;

		bool m_is_open;

		refs_t m_ids;
		Dict::auto_ptr_t m_values;
	};
}
