#pragma once

#include "val.hpp"

namespace haystack {
    /**
     * HNA is the singleton value used to indicate not available.
     *
     * @see <a href='http://project-haystack.org/doc/TagModel#tagKinds'>Project Haystack</a>
     */
    class Na : public Val
    {
        // disable copy ctor
        Na(const Na&);
        // disable assignment
        Na& operator = (const Na &other);
    public:
        Na() {};
        const Type type() const { return NA_TYPE; }

        /**
        default NA value
        */
        static const Na& NA;

        /**
        Encode as "na"
        */
        const std::string to_string() const;

        /**
        Encode as "NA"
        */
        const std::string to_zinc() const;

        /**
        Equality
        */
        bool operator == (const Na &b) const;
        bool operator == (const Val &other) const;
        bool operator > (const Val &other) const;
        bool operator < (const Val &other) const;

        auto_ptr_t clone() const;
    };
};
