// Copyright (c) 2015, J2 Innovations
// Copyright (c) 2012 Brian Frank
// Licensed under the Academic Free License version 3.0
// History:
//   09 Sep 2014  Radu Racariu<radur@2inn.com> Ported to C++
//   06 Jun 2011  Brian Frank  Creation

// Includes

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "zincreader.hpp"
#include "zincwriter.hpp"
#include "marker.hpp"
#include "bool.hpp"
#include "num.hpp"
#include "ref.hpp"
#include "uri.hpp"
#include "na.hpp"
#include "hisitem.hpp"
#include "datetimerange.hpp"
#include "op.hpp"
#include "dict.hpp"
#include "points.hpp"
#include "dict_type.hpp"
#include "logger.h"

#include <boost/scoped_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

#include <Poco/StringTokenizer.h>
#include <Poco/Net/DNS.h>
#include "Poco/Util/Application.h"

#include <sstream>
#include <stdexcept>
#include <string>       // std::string
#include <fstream>
#include <iostream>     // std::cout
#include <pthread.h>
#include <algorithm>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>   //ifreq
#include <unistd.h>   //close
// #include <plog/Log.h>

#include "engineio.h" //for temp_channel_add()

// Namespace

using namespace haystack;

// Definitions

#define MAX_HISTORY			120

#define TIMER_GRANULARITY	1000
#define TIMER_PERIOD		1

#define TIMER_WATCHES		60
#define TIMER_HISTORY		60

// Global data

const char * nanStr = "nan";

boost::ptr_map<std::string,Dict> PointServer::m_recs;
std::vector<std::string> PointServer::m_allowIPs;

Dict* PointServer::m_about = NULL;
std::vector<const Op*>* PointServer::m_ops = NULL;

// Local data

static std::string g_sEngineStatus[]={"ok","unknown","stale","disabled","fault","down"};
static std::string g_sEngineErrors[]={"","","","","Configuration errror","Hardware I/O error"};

// Sedona linkage

extern int willTerminate();

//////////////////////////////////////////////////////////////////////////
// PointServer class
//////////////////////////////////////////////////////////////////////////

// PointServer
// This class provides an impl of Server for Sedona and engine

PointServer::PointServer():
	m_timer(TIMER_GRANULARITY,TIMER_PERIOD*TIMER_GRANULARITY)
{
	struct stat buffer;
	Poco::Util::Application &app = Poco::Util::Application::instance();
	const std::string allowedIpListFile = std::string(app.config().getString("application.configDir")+"/AllowedCorsUrl.config");

	// Initialize Debug logs to file

	// if(stat(EACIO_FILE_LOG,&buffer)<0)
	// {
		//File might not exists
		// Create debug file
		// TODO: return value not checked
		// open(EACIO_FILE_LOG, O_RDWR | O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO);
		// std::cout << "-- MESSAGE [sys::Haystack] " << "Creating debug file at " << EACIO_FILE_LOG << std::endl;
		// LOG_INFO_HAYSTACKOPS_MSG("Creating debug file at:%s",EACIO_FILE_LOG);

	// }
	// plog::init(plog::debug, EACIO_FILE_LOG, 1000000, 1);

	// Read Allowed IP:Port list

	if(stat(allowedIpListFile.c_str(), &buffer)==0)
	{
		std::ifstream allowedIpFile(allowedIpListFile);
		std::copy(std::istream_iterator<std::string>(allowedIpFile),
					std::istream_iterator<std::string>(),
					std::back_inserter(m_allowIPs));
	}
	else
	{
		m_allowIPs.push_back("*");
		// std::cout << "-- MESSAGE [sys::Haystack] " << allowedIpListFile << " file not found. Adding '*' for Access-Control-Allow-Origin" << std::endl;
		LOG_WARNING_HAYSTACKOPS_MSG("%s file not found. Adding '*' for Access-Control-Allow-Origin",allowedIpListFile.c_str());
	}

	// Read points from zinc file and insert in map

	if(stat(std::string(app.config().getString("application.configDir")+"/database.zinc").c_str(), &buffer)==0)
	{
		std::ifstream t (std::string(app.config().getString("application.configDir")+"/database.zinc").c_str());
		std::string str;

		t.seekg(0, std::ios::end);
		str.reserve(t.tellg());
		t.seekg(0, std::ios::beg);

		str.assign((std::istreambuf_iterator<char>(t)),
						std::istreambuf_iterator<char>());

		std::istringstream iss(str);

		ZincReader r(iss);
		Grid::auto_ptr_t g = r.read_grid();
		//std::string out = ZincWriter::grid_to_string(*g);

		//Extract and Add all points from grid
		add_point_custom(*g);
	}
	else
	{
		// std::cout << "-- MESSAGE [sys::Haystack] " << EACIO_FILE_DATABASE_ZINC << " file not found. Type http://eacio_device_ip:8085/read?filter=error for more info" << std::endl;
		LOG_ERR_HAYSTACKOPS_MSG("%s file not found. Type http://eacio_device_ip:8085/read?filter=error for more info",std::string(app.config().getString("application.configDir")+"/database.zinc").c_str());
		char temp[200] = "add_points_file_at_";
		strcat (temp, std::string(app.config().getString("application.configDir")+"/database.zinc").c_str());

		Grid::auto_ptr_t g(new Grid());
		g->meta().add("err");
		g->add_col("error");
		g->add_col("resolution");

		Val* v[2] = {
			new Str("points_file_not_found"),
			new Str(temp)
			};
		g->add_row(v, 2);

		add_point_custom(*g);
	}

	//Initialize R/W lock. Used to protect map m_recs
	//pthread_rwlock_init(&rwlock, NULL);

    Poco::TimerCallback<PointServer> callback(*this,&PointServer::on_timer);
    m_timer.start(callback);
}

//
//

const std::vector<const Op *> &PointServer::ops()
{
	// lazy init
	if(m_ops!=NULL)
		return (std::vector<const Op *> &) *m_ops;

	// create op list
	std::vector<const Op *> *v=new std::vector<const Op *>();

	for(StdOps::ops_map_t::const_iterator it=StdOps::ops_map().begin(),end=StdOps::ops_map().end();it!=end;it++)
		v->push_back(it->second);

    return (std::vector<const Op *> &) *m_ops;
}

//
//

const Op *const PointServer::op(const std::string &name,bool checked) const
{
    const StdOps::ops_map_t &o_map=StdOps::ops_map();

	// try to find op match
    std::map<std::string,const Op *const>::const_iterator it=o_map.find(name);

	if(checked && it==o_map.end())
		throw std::runtime_error("Unknown op name");

	return (it!=o_map.end())?it->second:NULL;
}

//////////////////////////////////////////////////////////////////////////
// Itterators
//////////////////////////////////////////////////////////////////////////

Server::const_iterator PointServer::begin() const {return const_iterator(m_recs.begin());}
Server::const_iterator PointServer::end() const {return const_iterator(m_recs.end());}

//////////////////////////////////////////////////////////////////////////
// About
//////////////////////////////////////////////////////////////////////////

// on_about
//

const Dict &PointServer::on_about() const
{
	if(m_about!=NULL)
		return *m_about;

	m_about=new Dict();

	m_about->add("serverName",Poco::Net::DNS::hostName())
		.add("vendorName","AnkaLabs")
		//.add("vendorUri",Uri("http://project-haystack.org/"))
		.add("vendorUri",Uri("https://ankalabs.com/"))
		.add("productName","Project Sandstar")
		.add("productVersion","0.5")
		//.add("productUri",Uri("http://project-haystack.org/"));
		.add("productUri",Uri("http://project-sandstar.org/"));

	return *m_about;
}

//////////////////////////////////////////////////////////////////////////
// Read
//////////////////////////////////////////////////////////////////////////

// on_read_by_id
//

Dict::auto_ptr_t PointServer::on_read_by_id(const Ref &id) const
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	// find record with id
	// LOG_DEBUG << "navID: " << id.value;
	recs_t::const_iterator it=m_recs.find(id.value);

	if(it!=m_recs.end())
		return ((Dict *) it->second)->clone();

	return Dict::auto_ptr_t();
}

//////////////////////////////////////////////////////////////////////////
// Navigation
//////////////////////////////////////////////////////////////////////////

Grid::auto_ptr_t PointServer::on_nav(const std::string& nav_id) const
{
	LOG_INFO_HAYSTACKOPS_MSG("in PointServer::on_nav");
    // test database navId is record id
    Dict::auto_ptr_t base;
    if (!nav_id.empty()) base = read_by_id(Ref(nav_id));

    // map base record to site, equip, or point
    //std::string filter = "site";
	std::string filter = "device";

    if (base.get() != NULL)
    {
        //if (base->has("site")) filter = "equip and siteRef==" + base->id().to_code();
        //else if (base->has("equip")) filter = "point and equipRef==" + base->id().to_code();
		if (base->has("equip")) filter = "device and equipRef==" + base->id().to_code();
		else if (base->has("device")) filter = "point and deviceRef==" + base->id().to_code();
        else filter = "navNoChildren";
    }
	//LOG_DEBUG << "filter: " << filter;
	// read children of base record
	Grid::auto_ptr_t read=read_all(filter);

	Grid::auto_ptr_t res(new Grid);
	res->reserve_rows(read->num_rows());

	// copy columns
	for(size_t i=0;i<read->num_cols();i++)
	{
		const Col& srcCol = read->col(i);
		res->add_col(srcCol.name()).add(srcCol.meta());
	}

	// add a new colum
	res->add_col("navId");

	// copy rows
	for(Grid::const_iterator row=read->begin(),e=read->end();row!=e;row++)
	{
        boost::scoped_ptr<Val*> v(new Val*[res->num_cols()]);

        // copy each Val from the row
        for (size_t i = 0; i < res->num_cols() - 1; i++)
        {
            (v.get())[i] = new_clone(row->get(res->col(i).name(), false));
        }

        // add the new 'navId' tag
        (v.get())[res->num_cols() - 1] = new_clone(Str(row->id().value));

        // add the new row to the result grid
        res->add_row(v.get(), res->num_cols());
    }

    return res;
}

Dict::auto_ptr_t PointServer::on_nav_read_by_uri(const Uri& uri) const
{
    return Dict::auto_ptr_t();
}

//////////////////////////////////////////////////////////////////////////
// Watches
//////////////////////////////////////////////////////////////////////////

// on_watch_open
// Called to create watch list item

Watch::shared_ptr PointServer::on_watch_open(const std::string &dis)
{
	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

	// add watch to list
	Watch::shared_ptr w=boost::make_shared<PointWatch>(*this,dis);
	m_watches[w->id()]=w;

	return w;
}

// on_watches
// Called to generate list of current watches

const std::vector<Watch::shared_ptr> PointServer::on_watches()
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	// add each watch to result list
	std::vector<Watch::shared_ptr> v;

	for(watches_t::const_iterator it=m_watches.begin(),e=m_watches.end();it!=e;it++)
		v.push_back(it->second);

	return v;
}

// on_watch
// Called to fetch watch list given its id

Watch::shared_ptr PointServer::on_watch(const std::string &id)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	// because next line would add it!
	if(m_watches.find(id)==m_watches.end())
		return NULL;

	Watch::shared_ptr w=m_watches[id];

	return w;
}

//////////////////////////////////////////////////////////////////////////
// Point Write
//////////////////////////////////////////////////////////////////////////

// on_point_write_array
// Called to generate grid of current write levels

Grid::auto_ptr_t PointServer::on_point_write_array(const Dict &rec)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	// build response grid
	Grid::auto_ptr_t g(new Grid);

	g->add_col("level");
	g->add_col("levelDis");
	g->add_col("val");
	g->add_col("who");

	// must have channel
	if(!rec.has("channel"))
	{
		// error: channel tag not found
	    return g;
	}

	// get channel write levels?
	ENGINE_CHANNEL channel=(ENGINE_CHANNEL) rec.get_int("channel");
	CHANNEL_WRITELEVEL *levels;

	if(engineio_getwritelevels(channel,&levels)<0)
		throw std::runtime_error("Failed to find write levels");

	// set grid rows
	for(int n=0;n<MAX_WRITELEVELS;n++)
	{
		int level=n+1;

		char dis[16];
		snprintf(dis,15,"Level %d",level);

		Val *v[4]={
			new Num(level),
			new Str(dis),
			(levels[n].nUsed==0)?NULL:new Num(levels[n].fValue),
			new Str(levels[n].sWho)};

		g->add_row(v,4);
	}
    return g;
}

// on_point_write
// Called to set write level for point

void PointServer::on_point_write(const Dict &rec,int level,const Val &val,const std::string &who,const Num &dur)
{
	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

	// must have channel
	if(!rec.has("channel"))
	{
		// error: channel tag not found
		return;
	}

	// get level info
	int nUsed=0;

	double fValue=0.0;
	double fDuration=0.0;

	const char *sWho=who.c_str();

	if(!rec.has("kind"))
		throw std::runtime_error("kind tag has unknown value");

	std::string kind = rec.get_str("kind");

	if(kind=="Bool")
	{
		if(val==EmptyVal::DEF)          fValue=2.0; //SEDONA_BOOLTYPE_NULL;   //2
		else if(val==Bool(true))        fValue=1.0; //SEDONA_BOOLTYPE_TRUE;   //1
		else /*if(val==Bool(false))*/   fValue=0.0; //SEDONA_BOOLTYPE_FALSE;  //0

		fDuration=dur.value;
		nUsed=1;
	}
	else //It should be "Number"
	{
		if(val==EmptyVal::DEF)
		{
			//fValue=0.0;
			//TODO: Below is magic number and Important. Keep insync with Analog Sedona Component
			fValue = (-8889);
		}
		else
		{
			fValue=strtod(val.to_string().c_str(),NULL);
		}
		fDuration=dur.value;
		nUsed=1;
	}

	// set write level?
	ENGINE_CHANNEL channel=(ENGINE_CHANNEL) rec.get_int("channel");

	CHANNEL_WRITELEVEL *current;
	int curLevel;

// todo: does user have to match when setting level?

	if(engineio_setwritelevel(channel,level,nUsed,fValue,fDuration,sWho,&current,&curLevel)<0)
		throw std::runtime_error("Failed to set write levels");

	// new level reached?
	if(current!=NULL)
	{
		// std::cout << "-- MESSAGE [sys::Engine] write channel=" << channel << ", value=" << current->fValue << ", level=" << curLevel << std::endl;
		LOG_INFO_HAYSTACKOPS_MSG("write channel=%d ,values=%f, level=%d",channel,current->fValue,curLevel);

		// set write tags
		const Ref &ref=rec.get_ref("id");
		std::string id=ref.to_string();

		boost::ptr_map<std::string,Dict>::iterator p=m_recs.find(id);

		if(p!=m_recs.end())
		{
			Dict *d=(Dict *) p->second;

			if(d->has("writeVal")) d->update("writeVal",current->fValue,"");
			if(d->has("writeLevel")) d->update("writeLevel",curLevel,"");
			if(d->has("writeStatus")) d->update("writeStatus","unknown");
			if(d->has("writeErr")) d->update("writeErr","");
		}

		// write value to engine
		//ENGINE_VALUE value;

		//value.cur=(ENGINE_DATA) current->fValue;
		//value.flags=ENGINE_DATA_CUR;

		//engineio_write_channel(channel,&value);
	}
}

// engineWriteAck
// Call to update write status for channel

int PointServer::engineWriteAck(ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

	// find channel record
	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		// found the channel?
		Dict *d=(Dict *) it->second;
		if(!d->has("channel")) continue;

		ENGINE_CHANNEL c=(ENGINE_CHANNEL) d->get_int("channel");
		if(c!=channel) continue;

		// set write status
		std::string writeStatus=g_sEngineStatus[(int) value->status];
		std::string writeError=g_sEngineErrors[(int) value->status];

		//std::cout << "-- MESSAGE [sys::Engine] write ack channel=" << channel << ", status='" << writeStatus << "'" << std::endl;

// todo: update writeVal to actual written value?

		if(d->has("writeStatus")) d->update("writeStatus",writeStatus);
		if(d->has("writeErr")) d->update("writeErr",writeError);

		return 0;
	}
	// std::cout << "-- ERROR [sys::Engine] write ack channel=" << channel << " not found" << std::endl;
	LOG_ERR_HAYSTACKOPS_MSG("write ack channel=%d not found",channel);

	return -1;
}

//////////////////////////////////////////////////////////////////////////
// History
//////////////////////////////////////////////////////////////////////////

// on_his_read
// Called to generate list of history items

std::vector<HisItem> PointServer::on_his_read(const Dict &rec,const DateTimeRange &range)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	// get row id
	const Ref &ref=rec.get_ref("id");
	std::string id=ref.to_string();

	// find its history
	PointHistory::shared_ptr h=his_find(id);

	// fetch items in range
	std::vector<HisItem> his;
	h->Fetch(range,his);

	return his;
}

// on_his_write
// Called to write to history items

void PointServer::on_his_write(const Dict &rec,const std::vector<HisItem> &items)
{
    throw std::runtime_error("Unsupported Op 'on_his_write'");
}

// his_store
// Call to store history for a point

void PointServer::his_store(Dict *row,const std::string &type)
{
	// history wanted?
	if(row->get("his",false)!=Marker::VAL) return;

	// right type?
	std::string kind=row->has("kind")?row->get_str("kind"):"Number";
	std::string terp=row->has("hisInterpolate")?row->get_str("hisInterpolate"):((kind=="Number")?"linear":"cov");

	if(terp!=type) return;

	// get row id
	const Ref &ref=row->get_ref("id");
	std::string id=ref.to_string();

	// find its history
	PointHistory::shared_ptr h=his_find(id);

// todo: check hisInterval expired for linear

	// get current value and status
	const Val &curVal=row->get("curVal",false);

	std::string curStatus=row->get_str("curStatus");
	std::string curErr=row->get_str("curErr");

	// get timezone
	std::string tz=row->get_str("tz");
	TimeZone timeZone(tz);

	// totalize values?
	bool totalized=(row->get("hisTotalized",false)==Marker::VAL);

	// store current value
//	std::cout << "-- MESSAGE [sys::Haystack] history store id=" << id << std::endl;
	h->Store(curStatus,curVal,timeZone,totalized);

	// update history status
	row->update("hisStatus",curStatus);
	row->update("hisErr",curErr);
}

// his_find
// Call to find point history store

PointHistory::shared_ptr PointServer::his_find(const std::string &id)
{
	// find its history entry
	PointHistory::shared_ptr h;

	if(m_history.find(id)==m_history.end())
	{
		// not found, add to list
		h=boost::make_shared<PointHistory>(id);
		m_history[id]=h;
	}
	else h=m_history[id];

	return h;
}

//////////////////////////////////////////////////////////////////////////
// Actions
//////////////////////////////////////////////////////////////////////////

Grid::auto_ptr_t PointServer::on_invoke_action(const Dict& rec, const std::string& action, const Dict& args)
{
    // std::cout << "-- on_invoke_action '" << rec.dis() << "." << action << "' " << args.to_string() << std::endl;
    LOG_DEBUG_HAYSTACKOPS_MSG("on_invoke_action '%s.%s' %s",rec.dis().c_str(), action.c_str(),args.to_string().c_str());
    return Grid::auto_ptr_t();
}

//////////////////////////////////////////////////////////////////////////
// Impl
//////////////////////////////////////////////////////////////////////////

/*
void PointServer::add_site(const std::string& dis, const std::string& geoCity, const std::string& geoState, int area)
{
    Dict::auto_ptr_t site(new Dict);
    site->add("id", Ref(dis))
        .add("dis", dis)
        .add("site", Marker::VAL)
        .add("geoCity", geoCity)
        .add("geoState", geoState)
        .add("geoAddr", "" + geoCity + "," + geoState)
        .add("tz", "New_York")
        .add("area", Num(area, "ft\xc2\xb2"));


    add_meter(*site, dis + "-Meter");
    add_ahu(*site, dis + "-AHU1");
    add_ahu(*site, dis + "-AHU2");

    std::string k = dis;
    m_recs.insert(k, site);
}
*/

/*
void PointServer::add_meter(Dict& site, const std::string& dis)
{
    Dict::auto_ptr_t equip(new Dict);
    equip->add("id", Ref(dis))
        .add("dis", dis)
        .add("equip", Marker::VAL)
        .add("elecMeter", Marker::VAL)
        .add("siteMeter", Marker::VAL)
        .add("siteRef", site.get("id"));

    add_point(*equip, dis + "-KW", "kW", "elecKw");
    add_point(*equip, dis + "-KWH", "kWh", "elecKwh");

    std::string k = dis;
    m_recs.insert(k, equip);
}
*/

/*
void PointServer::add_ahu(Dict& site, const std::string& dis)
{
    Dict::auto_ptr_t equip(new Dict);
    equip->add("id", Ref(dis))
        .add("dis", dis)
        .add("equip", Marker::VAL)
        .add("ahu", Marker::VAL)
        .add("siteRef", site.get("id"));

    add_point(*equip, dis + "-Fan", "", "discharge air fan cmd");
    add_point(*equip, dis + "-Cool", "", "cool cmd");
    add_point(*equip, dis + "-Heat", "", "heat cmd");
    add_point(*equip, dis + "-DTemp", "\xE2\x84\x89", "discharge air temp sensor");
    add_point(*equip, dis + "-RTemp", "\xE2\x84\x89", "return air temp sensor");
    add_point(*equip, dis + "-ZoneSP", "\xE2\x84\x89", "zone air temp sp writable");

    std::string k = dis;
    m_recs.insert(k, equip);
}
*/

/*
void PointServer::add_point(Dict& equip, const std::string& dis, const std::string& unit, const std::string& markers)
{
    Dict::auto_ptr_t d(new Dict);

    d->add("id", Ref(dis))
        .add("dis", dis)
        .add("point", Marker::VAL)
        .add("his", Marker::VAL)
        .add("cur", Marker::VAL)
        .add("siteRef", equip.get("siteRef"))
        .add("equipRef", equip.get("id"))
        .add("kind", unit.empty() ? "Bool" : "Number")
        .add("tz", "New_York");

    if (!unit.empty()) d->add("unit", unit);

    //Poco::StringTokenizer st(markers, " ");

    //for (Poco::StringTokenizer::Iterator it = st.begin(), end = st.end(); it != end; ++it)
    //    d->add(*it);

    std::string k = dis;
    m_recs.insert(k, d);
}
*/

void PointServer::add_point_custom(Grid& g)
{
  std::string temp_pt_id = "channel-";
  std::string temp_pt_id2 = "index-";
  std::string pt_id = temp_pt_id;

  // Variables to hold total number of row and columns
  // of grid read from zinc file
  const size_t n_rows = g.num_rows();
  const size_t n_cols = g.num_cols();

  /* TBD: What if n_rows and n_cols are zero or very big number?
   * Please place a limit
   */

  // Create a dict of each row
  for (size_t i = 0; i < n_rows; i++)
  {
     Dict::auto_ptr_t d(new Dict);
     for (size_t j = 0; j < n_cols; j++)
     {
        const Row& r = g.row(i);
        //Add to dict (col, row)
        const Col& c = g.col(j);
        d->add(c.name(), r.get(c));
     }

	 if (d->has("id"))
	 {
		// Id 'id' tag available, use this id value to insert dict in map
	    const Ref& ref = d->get_ref("id");
		pt_id = ref.to_string();
	 }
	 else
	 {
	 	// otherwise, use channel<n>
	 	if(d->has("channel"))
	 	{
			int ch=(int) d->get_int("channel");
			pt_id=temp_pt_id+boost::lexical_cast<std::string>(ch);
	 	}
	 	else pt_id = temp_pt_id2 + boost::lexical_cast<std::string>(i);
	 }

     // Add Each row dict as point
	 //LOG_DEBUG << "id: " << pt_id;
     m_recs.insert(pt_id, d);
  }
}

// Read curVal of given point.
int PointServer::readCurVal (const int channel, double * curVal)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		// found the channel?
		Dict *row = (Dict *) it->second;
		if(!row->has("channel")) continue;

		ENGINE_CHANNEL c = (ENGINE_CHANNEL) row->get_int("channel");
		if(c!=channel) continue;

		//LOG_DEBUG << "Channel Found " << channel;

		if (row->hasTag("curVal"))
		{
			//LOG_DEBUG << "curVal found at channel " << channel;
			//*curVal=row->get_double("curVal");

			// get curVal value
			const Val &val=row->get("curVal",false);

			switch(val.type())
			{
				case Val::BOOL_TYPE:
					{
						// bool type
						const Bool *boolVal=(Bool *) &val;
						*curVal=boolVal->value?1.0:0.0;
					}
					break;

				case Val::NUM_TYPE:
					{
						// number type
						const Num *numVal=(Num *) &val;
						*curVal=numVal->value;
					}
					break;

				// todo: add STR_TYPE, check for enum tag, set curVal to enum index?

				default:
					// others...
					*curVal=0.0;
					break;
			}
			//LOG_DEBUG << "channel: " << channel << " value: " << *curVal;
			return 0;
		}
	}
	return -1;
}

int sedonaReadCurVal(const int channel, double * curVal)
{
	return PointServer::readCurVal(channel, curVal);
}

// Write curVal of given point.
void PointServer::writeCurVal (const int channel, double value)
{
	int c = 0;

	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);
	//boost::ptr_map<std::string, Dict>::iterator p

	//DBG
	//LOG_DEBUG << "Channel: " << channel << "  value: " << value;

	for (recs_t::const_iterator it = m_recs.begin(), e = m_recs.end();
			it != e;
			++it)
	{
		// found the channel?
		Dict *row = (Dict *) it->second;
		if(!row->has("channel")) continue;

		c = row->get_int("channel");

		if (channel == c)
		{
			//Dict::auto_ptr_t d = row->clone();

			//if (d->hasTag("curVal"))
			if (row->hasTag("curVal"))
			{
				//DBG
				//LOG_DEBUG << "curVal found at channel " << c;

				// Update zinc database
				//Get kind (default 'Number')
				//std::string kind = (d->hasTag("kind")?(d->get_str("kind")):("Number"));
				std::string kind = (row->hasTag("kind")?(row->get_str("kind")):("Number"));

				//Update as per kind
				if(kind=="Bool")
				{
					//d->update("curVal", ((value!=0)?Bool(true):Bool(false)));
					row->update("curVal", ((value!=0)?Bool(true):Bool(false)));
				}
				else
				{
					//default kind is Num
					//d->update("curVal", value, "");
					row->update("curVal", value, "");
				}

				//std::string ch_id = it->first;
				//boost::ptr_map<std::string, Dict>::iterator p = m_recs.find(ch_id);

				//m_recs.erase(p);
				//m_recs.insert(ch_id, d);

				// Write value to Engine
				ENGINE_VALUE eng_value;

				eng_value.cur=(ENGINE_DATA) value;
				eng_value.flags=ENGINE_DATA_CUR;

				//DBG
				//LOG_DEBUG << "Send Msg";

				engineio_write_channel(channel,&eng_value);

				//DBG
				//LOG_DEBUG << "Msg Send Done";
			}
		}
	}
}

void sedonaWriteCurVal(const int channel, double value)
{
	PointServer::writeCurVal(channel, value);
}

//void PointServer::writeVirtualVal (const int channel, double value)
//{
//	int c = 0;
//
//	// __WRITE_LOCK__
//	Poco::ScopedWriteRWLock l(m_lock);
//
//	//TODO: Separate virtual channels from m_recs.
//
//	for (recs_t::const_iterator it = m_recs.begin(), e = m_recs.end();
//			it != e;
//			++it)
//	{
//		// found the channel?
//		Dict *row = (Dict *) it->second;
//		if(!row->has("channel")) continue;
//
//		c = row->get_int("channel");
//
//		if (channel == c)
//		{
//			Dict::auto_ptr_t d = row->clone();
//
//			if(d->get("virtualChannel",false)!=Marker::VAL)
//			{
//				LOG_DEBUG << "Channel " << channel << " is not virtual";
//				return;
//			}
//
//			if (d->hasTag("curVal"))
//			{
//				// Update zinc database
//				//Get kind (default 'Number')
//				std::string kind = (d->hasTag("kind")?(d->get_str("kind")):("Number"));
//
//				//Update as per kind
//				if(kind=="Bool")
//				{
//					d->update("curVal", ((value!=0)?Bool(true):Bool(false)));
//				}
//				else
//				{
//					//default kind is Num
//					d->update("curVal", value, "");
//				}
//
//				std::string ch_id = it->first;
//				boost::ptr_map<std::string, Dict>::iterator p = m_recs.find(ch_id);
//
//				m_recs.erase(p);
//				m_recs.insert(ch_id, d);
//
//				// Write value to Engine
//				ENGINE_VALUE eng_value;
//
//				eng_value.cur=(ENGINE_DATA) value;
//				eng_value.flags=ENGINE_DATA_CUR;
//
//				//engineio_write_virtual(channel,&eng_value);
//				engineio_write_channel(channel,&eng_value);
//			}
//		}
//	}
//}
//
//void sedonaWriteVirtualVal(const int channel, double value)
//{
//	PointServer::writeVirtualVal(channel, value);
//}

void PointServer::update_point (const Grid& g)
{
	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

//	int retry = 0;
//	int rc    = 0;
	const size_t n_cols = g.num_cols();

	// Loop: Extract Row from grid
	for (Grid::const_iterator it = g.begin(), e = g.end(); it != e; ++it)
	{
		const std::string& strID = it->get_ref("id").to_string();

//		for (retry = 0; retry < MAX_RETRY; retry++)
//		{
//			rc = pthread_rwlock_trywrlock(&rwlock);

//			if (rc == EBUSY)
//			{
//				// TODO: Continue after few millisec delay
//				continue;
//			}
//			else if (rc == 0)
//			{
				boost::ptr_map<std::string, Dict>::iterator p = m_recs.find(strID);
				Dict::auto_ptr_t d = ((Dict*)p->second)->clone();

				for (size_t j = 0; j < n_cols; j++)
				{
					const Col& c = g.col(j);
					const std::string& colName = c.name();

					if (colName != "id")
					{
						//if (d->has(colName))
						//{
							d->update(c.name(), it->get(c));
						//}
						//else
						//{
						//	d->add(c.name(), it->get(c));
						//}
					}
				}

				// Add Each row dict as point
				m_recs.erase(p);
				m_recs.insert(strID, d);

//				if (0 != pthread_rwlock_unlock(&rwlock))
//				{
//				}

				// Exit retry loop
//				break;
//			}
//		}

//		if (retry >= MAX_RETRY)
//		{
//			throw std::runtime_error("UPDATE POINT: Retry exceeded");
//		}
	}
}

void updatePoint (const Grid& g)
{
	PointServer::update_point(g);
}

void PointServer::add_point(const Grid& g)
{
	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

	// Variables to hold total number of row and columns
	// of grid read from zinc file
	const size_t n_rows = g.num_rows();
	const size_t n_cols = g.num_cols();

	// Sanity check
	if ((n_rows <= 0) || (n_cols <= 0))
	{
		throw std::runtime_error("ADD POINT: invalid row & col numbers");
	}

	// Create a dict of each row
	for (size_t i = 0; i < n_rows; i++)
	{
//		int rc = 0;
//		int retry = 0;

		Dict::auto_ptr_t d(new Dict);

		//LOG_DEBUG << "iLoop: " << i;
		for (size_t j = 0; j < n_cols; j++)
		{
			const Row& r = g.row(i);
			//Add to dict (col, row)
			const Col& c = g.col(j);
			//LOG_DEBUG << "jLoop: " << j;
			d->add(c.name(), r.get(c));
		}

		// TODO: Error check on strID
		const std::string& strID = d->get_ref("id").to_string();
		//LOG_DEBUG << "Insert ID: " << strID;

		// retry loop
//		for (retry = 0; retry < MAX_RETRY; retry++)
//		{
//			rc = pthread_rwlock_trywrlock(&rwlock);

//			if (rc == EBUSY)
//			{
				// TODO: Continue after few millisec delay
//				continue;
//			}
//			else if (rc == 0)
//			{
				// Add Each row dict as point
				m_recs.insert(strID, d);

//				if (0 != pthread_rwlock_unlock(&rwlock))
//				{
//				}

//				break;
//			}
//		}

//		if (retry >= MAX_RETRY)
//		{
//			throw std::runtime_error("ADD POINT: Retry exceeded");
//		}
	}
}

void addPoint(const Grid& g)
{
	PointServer::add_point(g);
}

void PointServer::delete_point(const Grid& g)
{
	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

//	int rc = 0;
	std::string errStr = "";

	for (Grid::const_iterator it = g.begin(), e = g.end(); it != e; ++it)
	{
		const std::string& strID = it->get_ref("id").to_string();

		// retry loop
//		int retry = 0;
//		for (retry = 0; retry < MAX_RETRY; retry++)
//		{
//			rc = pthread_rwlock_trywrlock(&rwlock);

//			if (rc == EBUSY)
//			{
//				// TODO: Continue after few millisec delay
//				continue;
//			}
//			else if (rc == 0)
//			{
				boost::ptr_map<std::string, Dict>::iterator p = m_recs.find(strID);
				if (p != m_recs.end())
				{
					m_recs.erase(p);
				}
				else
				{
					errStr += (strID + ", ");
				}

//				if (0 != pthread_rwlock_unlock(&rwlock))
//				{
//				}

				// Exit from retry loop
//				break;
//			}
//		}

//		if (retry >= MAX_RETRY)
//		{
//			throw std::runtime_error("DELETE POINT: Retry exceeded");
//		}
	}

	if (errStr != "")
	{
		throw std::runtime_error("Channel ID: " + errStr + " not present in map");
	}
}

void deletePoint(const Grid& g)
{
	PointServer::delete_point(g);
}


t_ErrOp PointServer::optimize_grid()
{
	Grid::auto_ptr_t recordGrid;
	std::vector<std::string> columns;

	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

	// Create grid of map records
	recordGrid=Grid::make(m_recs);

	// Edit grid to remove blank columns

	size_t n_rows = recordGrid->num_rows();
	size_t n_cols = recordGrid->num_cols();

	if ((n_cols<=0) || (n_rows<=0))
	{
		//LOG_DEBUG << "n_rows: " << n_rows;
		//LOG_DEBUG << "n_cols: " << n_cols;
		return ERR_OP_INPUTS;
	}

	for (size_t i = 0; i < n_cols; i++)
	{
		size_t j = 0;
		const Col& c = recordGrid->col(i);

		for (j = 0; j < n_rows; j++)
		{
			const Row& r = recordGrid->row(j);

			if (EmptyVal::DEF != r.get(c))
			{
				break;
			}
		}

		// All empty ?
		if(j==n_rows)
		{
			columns.push_back(c.name());
			//LOG_DEBUG << "To remove: " << c.name();
		}
	}

	for(std::vector<std::string>::iterator it=columns.begin(), e=columns.end(); it!=e; it++)
	{
		recordGrid->remove_col(*it);
	}

	std::string temp_pt_id = "channel-";
	std::string temp_pt_id2 = "index-";
	std::string pt_id = temp_pt_id;

	// Variables to hold total number of row and columns
	// of grid read from zinc file
	n_rows = recordGrid->num_rows();
	n_cols = recordGrid->num_cols();

	//LOG_DEBUG << "n_rows: " << n_rows;
	//LOG_DEBUG << "n_cols: " << n_cols;

	// Create a dict of each row
	for (size_t i = 0; i < n_rows; i++)
	{
		Dict::auto_ptr_t d(new Dict);
		for (size_t j = 0; j < n_cols; j++)
		{
			const Row& r = recordGrid->row(i);
			//Add to dict (col, row)
			const Col& c = recordGrid->col(j);
			d->add(c.name(), r.get(c));
		}

		if (d->has("id"))
		{
			// Id 'id' tag available, use this id value to insert dict in map
			const Ref& ref = d->get_ref("id");
			pt_id = ref.to_string();
		}
		else
		{
			// otherwise, use channel<n>
			if(d->has("channel"))
			{
				int ch=(int) d->get_int("channel");
				pt_id=temp_pt_id+boost::lexical_cast<std::string>(ch);
			}
			else pt_id = temp_pt_id2 + boost::lexical_cast<std::string>(i);
		}

		// Add Each row dict as point
		m_recs.erase(pt_id);
		m_recs.insert(pt_id, d);
	}

	return ERR_OP_OK;
}

t_ErrOp optimizeGrid()
{
	return PointServer::optimize_grid();
}

void PointServer::commit_zinc(void)
{
	Poco::Util::Application &app = Poco::Util::Application::instance();
	Grid::auto_ptr_t recordGrid;

	// Create grid of map records
	recordGrid=Grid::make(m_recs);

	// Clear and Open zinc file to edit
	std::ofstream fZinc (std::string(app.config().getString("application.configDir")+"/database.zinc").c_str(), std::ios::out | std::ios::trunc);

	// Open ZincWriter
	ZincWriter w(fZinc);

	// Paste record grid to zinc file
	w.m_nulldata=true;
	w.write_grid(*recordGrid);

	// Exit
	return;
}

void commitZinc()
{
	PointServer::commit_zinc();
}

int PointServer::resolveMarkerList(char * markerList)
{
	int channel = INVALID_CHANNEL_ID;

	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	// Marker list BLANK ?
	if (0 == strcmp (markerList, ""))
	{
		//LOG_DEBUG << "Error: MarkerList Blank";
		return channel;
	}

	std::istringstream markerStream(markerList);
	std::string tagValPair;
	std::vector<std::string> vecTag;
	boost::ptr_vector<Val> vecValue;

	// Collect tag-value pair into vector
	while(std::getline(markerStream, tagValPair, ','))
	{
		//LOG_DEBUG << "Info: TagVal: " << tagValPair;

		std::string tag;
		std::string value;

		// Separate TAG and its VALUE
		std::istringstream tagValPairStream(tagValPair);

		//Get TAG
		std::getline(tagValPairStream, tag, ':');

		// Get VALUE
		if(!tagValPairStream.eof())
		{
			std::getline(tagValPairStream, value);
			std::istringstream iss(value);
			ZincReader r(iss);

			try
			{
				vecValue.push_back(r.read_val());
			}
			catch (const std::exception& e)
			{
				// LOG_DEBUG << "ERROR: " << e.what();
				LOG_ERR_HAYSTACKOPS_MSG("ERROR:%s",e.what());
				return 0;
			}
		}
		else
		{
			vecValue.push_back(Marker::VAL.clone());
		}

		//LOG_DEBUG << "tag: " << tag;
		vecTag.push_back(tag);
	}

	// Parse each channel row of zinc grid to search a channel that has all
	// marker list provided to this function.
	for (recs_t::const_iterator it = m_recs.begin(), e = m_recs.end(); it != e; ++it)
	{
		std::vector<std::string>::iterator tagIdx = vecTag.begin();
		std::vector<std::string>::iterator end = vecTag.end();
		boost::ptr_vector<Val>::const_iterator valIdx = vecValue.begin();

		for (; tagIdx != end; tagIdx++, valIdx++)
		{
			if ((((Dict*)it->second)->get(*tagIdx, false)) != *valIdx)
			{
				// Exit checking
				break;
			}
		}

		if (tagIdx == end)
		{
			// found the channel?
			Dict *row=(Dict *) it->second;
			if(!row->has("channel")) continue;

			channel = (int) row->get_int("channel");
			//std::cout << "DBG: channel: " << channel << std::endl;
			return channel;
		}
	}

//	LOG_DEBUG << "marker not found";
	return channel;
}

int sedonaResolveChannel(char* markerList)
{
//	LOG_DEBUG << "MarkerList: " << markerList;
	return PointServer::resolveMarkerList(markerList);
//	LOG_DEBUG << "Channel: " << channel;
}

int PointServer::getRecordCount(char * markerList)
{
	int recCount = 0;

	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	// Marker list BLANK ?
	if (0 == strcmp (markerList, ""))
	{
		// LOG_DEBUG << "Error: MarkerList Blank";
		LOG_ERR_HAYSTACKOPS_MSG("ERROR:MarkerList Blank");
		return recCount;
	}

	std::istringstream markerStream(markerList);
	std::string tagValPair;
	std::vector<std::string> vecTag;
	boost::ptr_vector<Val> vecValue;

	// Collect tag-value pair into vector
	while(std::getline(markerStream, tagValPair, ','))
	{
		//LOG_DEBUG << "Info: TagVal: " << tagValPair;

		std::string tag;
		std::string value;

		// Separate TAG and its VALUE
		std::istringstream tagValPairStream(tagValPair);

		//Get TAG
		std::getline(tagValPairStream, tag, ':');

		// Get VALUE
		if(!tagValPairStream.eof())
		{
			std::getline(tagValPairStream, value);
			std::istringstream iss(value);
			ZincReader r(iss);

			try
			{
				vecValue.push_back(r.read_val());
			}
			catch (const std::exception& e)
			{
				// LOG_DEBUG << "ERROR: " << e.what();
				LOG_ERR_HAYSTACKOPS_MSG("ERROR:%s",e.what());
				return 0;
			}
		}
		else
		{
			vecValue.push_back(Marker::VAL.clone());
		}

		//LOG_DEBUG << "tag: " << tag;
		vecTag.push_back(tag);
	}

	// Get number of points having given marker lists
	for (recs_t::const_iterator it = m_recs.begin(), e = m_recs.end(); it != e; ++it)
	{
		std::vector<std::string>::iterator tagIdx = vecTag.begin();
		std::vector<std::string>::iterator end = vecTag.end();
		boost::ptr_vector<Val>::const_iterator valIdx = vecValue.begin();

		for (; tagIdx != end; tagIdx++, valIdx++)
		{
			if ((((Dict*)it->second)->get(*tagIdx, false)) != *valIdx)
			{
				// Exit checking
				break;
			}
		}

		if (tagIdx == end)
		{
			recCount++;
		}
	}

	return recCount;
}

int sedonaGetRecordCount(char* markerList)
{
//	LOG_DEBUG << "MarkerList: " << markerList;
	return PointServer::getRecordCount(markerList);
//	LOG_DEBUG << "Channel: " << channel;
}

int PointServer::getCurStatus(const int channel, char * curStatus)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	// Sedona: Zero -> False and Non-Zero -> True
	int retVal = 0;

	if (curStatus == NULL)
		return retVal;

	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		// found the channel?
		Dict *row=(Dict *) it->second;
		if(!row->has("channel")) continue;

		ENGINE_CHANNEL c = (ENGINE_CHANNEL) row->get_int("channel");
		if(c!=channel) continue;

		// Should have "point" tag
		if (((row->get("point", false)) == Marker::VAL) || ((row->get("virtualChannel", false)) == Marker::VAL))
		{
			// Should have "curStatus" tag
			if (row->has("curStatus"))
			{
				strcpy (curStatus, (row->get_str("curStatus")).c_str());
				retVal = 1; //True
			}
		}

		// Exit
		break;
	}

	if (retVal == 0)
	{
		// If error, write un-defined value to curStatus
		strcpy(curStatus, "na");
	}

	return retVal;
}

int sedonaGetCurStatus(const int channel, char* curStatus)
{
	return PointServer::getCurStatus(channel, curStatus);
}

int PointServer::getChannelName(const int channel, char * channelName)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	// Sedona: Zero -> False and Non-Zero -> True
	int retVal = 0;

	if (channelName == NULL)
		return retVal;

	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		// found the channel?
		Dict *row=(Dict *) it->second;
		if(!row->has("channel")) continue;

		ENGINE_CHANNEL c = (ENGINE_CHANNEL) row->get_int("channel");
		if(c!=channel) continue;

		// Should have "point" tag
		if (((row->get("point", false)) == Marker::VAL) || ((row->get("virtualChannel", false)) == Marker::VAL))
		{
			// Should have "channelName" tag
			if (row->has("channelName"))
			{
				strcpy (channelName, (row->get_str("channelName")).c_str());
				retVal = 1; //True
			}
		}

		// Exit
		break;
	}

	if (retVal == 0)
	{
		// If error, write un-defined value to channelName
		strcpy(channelName, "");
	}

	return retVal;
}

// Get channel name
int sedonaGetChannelName (const int channel, char* channelName)
{
	return PointServer::getChannelName(channel, channelName);
}

// Write Component Id to point.
void PointServer::writeComponentId (const int channel, double id)
{
	int c = 0;

	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

	//DBG
	//LOG_DEBUG << "Channel: " << channel << "  id: " << id;

	for (recs_t::const_iterator it = m_recs.begin(), e = m_recs.end();
			it != e;
			++it)
	{
		// found the channel?
		Dict *row=(Dict *) it->second;
		if(!row->has("channel")) continue;

		c = row->get_int("channel");

		if (channel == c)
		{
			Dict::auto_ptr_t d = ((Dict*)it->second)->clone();

			if (d->has("sedonaId"))
			{
				// Update zinc database
				d->update("sedonaId", id, "");

				std::string ch_id = it->first;
				boost::ptr_map<std::string, Dict>::iterator p = m_recs.find(ch_id);

				m_recs.erase(p);
				m_recs.insert(ch_id, d);
			}
		}
	}
}

void sedonaWriteComponentId(const int channel, double id)
{
	PointServer::writeComponentId(channel, id);
}

// Write Component Type to point.
void PointServer::writeComponentType (const int channel, char* kit, char* name)
{
	int c = 0;

	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

	if ((kit==NULL) || (name==NULL))
	{
		return;
	}

	std::string type(kit);
	type = type+"::"+name;

	//DBG
	//LOG_DEBUG << "Channel: " << channel << " Type: " << type << " kit: " << kit << " name: " << name;

	for (recs_t::const_iterator it = m_recs.begin(), e = m_recs.end();
			it != e;
			++it)
	{
		// found the channel?
		Dict *row=(Dict *) it->second;
		if(!row->has("channel")) continue;

		c = row->get_int("channel");

		if (channel == c)
		{
			Dict::auto_ptr_t d = row->clone();

			if (d->has("sedonaType"))
			{
				// Update zinc database
				d->update("sedonaType", type);

				std::string ch_id = it->first;
				boost::ptr_map<std::string, Dict>::iterator p = m_recs.find(ch_id);

				m_recs.erase(p);
				m_recs.insert(ch_id, d);
			}
		}
	}
}

void sedonaWriteComponentType(const int channel, char* kit, char* name)
{
	PointServer::writeComponentType(channel, kit, name);
}

bool PointServer::isChannelEnabled(const int channel)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	bool retVal = false;

	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		// found the channel?
		Dict *row=(Dict *) it->second;
		if(!row->has("channel")) continue;

		ENGINE_CHANNEL c = (ENGINE_CHANNEL) row->get_int("channel");
		if(c!=channel) continue;

		// Check "enabled" tag
		if ((row->get("enabled", false)) == Marker::VAL)
		{
			retVal = true;
		}

		// Exit
		break;
	}

	return retVal;
}

bool sedonaIsChannelEnabled(const int channel)
{
	return PointServer::isChannelEnabled(channel);
}

bool PointServer::getBoolTagValue(int channel, char* tag)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	bool retVal = false;

	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		// found the channel?
		Dict *row=(Dict *) it->second;
		if(!row->has("channel")) continue;

		ENGINE_CHANNEL c = (ENGINE_CHANNEL) row->get_int("channel");
		if(c!=channel) continue;

		//LOG_DEBUG << "channel: " << c;
		//LOG_DEBUG << "tag: " << tag;

		const Val& v = row->get(tag, false);
        if (v.type() == Val::BOOL_TYPE)
		{
			retVal = ((Bool&)v).value;
			//LOG_DEBUG << (((Bool&)v).value?"true":"false");
		}

		// Exit
		break;
	}

	return retVal;
}

bool sedonaGetBoolTagValue(int channel, char* tag)
{
	return PointServer::getBoolTagValue(channel, tag);
}

float PointServer::getNumberTagValue(int channel, char* tag)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	float retVal = 0.0;

	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		// found the channel?
		Dict *row=(Dict *) it->second;
		if(!row->has("channel")) continue;

		ENGINE_CHANNEL c = (ENGINE_CHANNEL) row->get_int("channel");
		if(c!=channel) continue;

		//LOG_DEBUG << "channel: " << c;
		//LOG_DEBUG << "tag: " << tag;

		const Val& v = row->get(tag, false);
        if (v.type() == Val::NUM_TYPE)
		{
			retVal = ((Num&)v).value;
			//LOG_DEBUG << ((Num&)v).value;
		}

		// Exit
		break;
	}

	return retVal;
}

float sedonaGetNumberTagValue(int channel, char * tag)
{
	return PointServer::getNumberTagValue(channel, tag);
}

void PointServer::getStringTagValue(int channel, char * tag, char * val)
{
	// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		// found the channel?
		Dict *row=(Dict *) it->second;
		if(!row->has("channel")) continue;

		ENGINE_CHANNEL c = (ENGINE_CHANNEL) row->get_int("channel");
		if(c!=channel) continue;

		//LOG_DEBUG << "channel: " << c;
		//LOG_DEBUG << "tag: " << tag;

		const Val& v = row->get(tag, false);
        if (v.type() == Val::STR_TYPE)
		{
			strcpy (val, (((Str&)v).value).c_str());
			// LOG_DEBUG << ((Str&)v).value;
			Logger::getInstance().debug("%s",((Str&)v).value.c_str());
		}

		// Exit
		break;
	}

	return;
}

void sedonaGetStringTagValue(int channel, char * tag, char * val)
{
	PointServer::getStringTagValue(channel, tag, val);
	return;
}

char PointServer::getTagType(int channel, char* tag)
{
		// __READ_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	char retVal = 0;

	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		// found the channel?
		Dict *row=(Dict *) it->second;
		if(!row->has("channel")) continue;

		ENGINE_CHANNEL c = (ENGINE_CHANNEL) row->get_int("channel");
		if(c!=channel) continue;

		const Val& v = row->get(tag, false);
        if (v.type() == Val::BOOL_TYPE)
		{
			retVal = 1;
		}
		else if (v.type() == Val::NUM_TYPE)
		{
			retVal = 2;
		}
		else if (v.type() == Val::STR_TYPE)
		{
			retVal = 3;
		}

		// Exit
		break;
	}

	return retVal;
}

char sedonaGetTagType(int channel, char * tag)
{
	return PointServer::getTagType(channel, tag);
}

int PointServer::getWriteLevels(int channel)
{
	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

	CHANNEL_WRITELEVEL *levels;
	int level=0;

	if (engineio_getwritelevels(channel, &levels)<0)
	{
		return 0;
	}

	for(int n=0;n<MAX_WRITELEVELS;n++)
	{
		if(levels[n].nUsed==1)
		{
			level=n+1;
			levels[n].nUsed=0;
			break;
		}
	}

	return level;
}

int sedonaGetWriteLevels(int channel)
{
	return PointServer::getWriteLevels(channel);
}

double PointServer::getwritelevelsvalue(int channel, int level)
{
	// __WRITE_LOCK__
	Poco::ScopedReadRWLock l(m_lock);

	CHANNEL_WRITELEVEL *levels;

	if (engineio_getwritelevels(channel, &levels)<0)
	{
		return 0.0;
	}

	return levels[level-1].fValue;
}

double sedonaGetWriteLevelsValue(int channel, int level)
{
	return PointServer::getwritelevelsvalue(channel, level);
}

//////////////////////////////////////////////////////////////////////////
// Timer
//////////////////////////////////////////////////////////////////////////

// on_timer
// Called periodically every 1 sec

void PointServer::on_timer(Poco::Timer &timer)
{
	static int count=1;

	// ignore if terminating
	if(willTerminate()) return;

	// update point values
//	std::cout << "-- MESSAGE [sys::Engine] update timer" << std::endl;
	timer_update();

	// every 1 min
	if(count%TIMER_WATCHES==0)
	{
//		std::cout << "-- MESSAGE [sys::Haystack] watch timer" << std::endl;
		timer_watches();
	}

	// every 1 min
	if(count%TIMER_HISTORY==0)
	{
//		std::cout << "-- MESSAGE [sys::Haystack] history timer" << std::endl;
		timer_history();
	}

	count++;
}

// timer_update
// Called to update database with live values

void PointServer::timer_update()
{
	// enum current values
	ENGINE_CHANNEL channel;
	ENGINE_VALUE value;

	int cenum=-1;
	engineio_enum_start(&cenum);

	while(engineio_enum_next(&cenum,&channel,&value)>=0)
	{
		// __WRITE_LOCK__
		Poco::ScopedWriteRWLock l(m_lock);

		// find channel record
		for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
		{
			// found the channel?
			Dict *row=(Dict *) it->second;
			if(!row->has("channel")) continue;

			ENGINE_CHANNEL c=(ENGINE_CHANNEL) row->get_int("channel");
			if(c!=channel) continue;

//			std::cout << "-- MESSAGE [sys::Engine] update channel=" << channel << ", value=" << value.cur << std::endl;

			// get cur and raw value
			double curVal=(double) value.cur;
			double rawVal=(double) value.raw;

			// raw value wanted?
			if(row->get("raw",false)==Marker::VAL)
				row->update("rawVal",rawVal,"");

			// cur value wanted?
			if(row->get("cur",false)!=Marker::VAL)
				break;

			// get cur status and error
			std::string curStatus=g_sEngineStatus[(int) value.status];
			std::string curErr=g_sEngineErrors[(int) value.status];

			// get format tags
			std::string kind=row->has("kind")?row->get_str("kind"):"Number";
			std::string unit=row->has("unit")?row->get_str("unit"):"";
			std::string list=row->has("enum")?row->get_str("enum"):"";

			bool valid=false;

			// number kind?
			if(kind=="Number")
			{
				// set number and unit
				row->update("curVal",curVal,unit);
				valid=true;
			}
			else
			{
				// bool kind?
				if(kind=="Bool")
				{
					// set bool
					row->update("curVal",Bool(curVal!=0.0));
					valid=true;
				}
				else
				{
					// string kind?
					if(kind=="Str")
					{
						// set string
						row->update("curVal",Num(curVal).to_zinc());
						valid=true;
					}
					else
					{
						// enumerated list?
						if(!list.empty())
						{
							// find enum item
							std::istringstream iss(list);
							std::string item;

							int i=(int) curVal;
							int n=0;

							while(std::getline(iss,item,','))
							{
								if(n==i)
								{
									// set enum string
									row->update("curVal",Str(item));
									valid=true;

									break;
								}
								n++;
							}
						}
					}
				}
			}

			// valid convertion?
			if(!valid)
			{
				// something went wrong
				curStatus="fault";
				curErr="Configuration error";

				row->update("curVal",Na());
			}

			// set cur status and error
			row->update("curStatus",curStatus);
			row->update("curErr",curErr);

			// store cov history
			his_store(row,"cov");
			break;
		}
	}

	// end update
	engineio_enum_end();
}

// timer_watches
// Called to detect watches with expired lease times

void PointServer::timer_watches()
{
	// detect garbage watches
	std::vector<Watch::shared_ptr> del;
	{
		// __READ_LOCK__
		Poco::ScopedReadRWLock l(m_lock);

		// look through watches
		for(watches_t::const_iterator it=m_watches.begin(),e=m_watches.end();it!=e;it++)
		{
			PointWatch &w=(PointWatch &) *it->second.get();

			// watch is open?
			if(w.is_open() && w.lease()>0)
			{
				// decrease lease time
				int lease=w.lease();

				lease-=TIMER_WATCHES;
				w.lease(lease);

				// delete if lease is up
				if(lease<=0)
				{
					// std::cout << "-- MESSAGE [sys::Haystack] watch expired '" << w.id() << "'" << std::endl;
					LOG_INFO_HAYSTACKOPS_MSG("watch expired '%s'",w.id().c_str());
					del.push_back(it->second);
				}
			}

			// watch is closed?
			if(!w.is_open() && w.lease()>0)
			{
				// std::cout << "-- MESSAGE [sys::Haystack] watch closed '" << w.id() << "'" << std::endl;
				LOG_INFO_HAYSTACKOPS_MSG("watch closed '%s'",w.id().c_str());
				del.push_back(it->second);
			}
		}
	}

	// found watches to delete?
	if(!del.empty())
	{
		// __WRITE_LOCK__
		Poco::ScopedWriteRWLock l(m_lock);

		// delete watches
		for(std::vector<Watch::shared_ptr>::const_iterator it=del.begin(),e=del.end();it!=e;it++)
		{
			watches_t::iterator pos=m_watches.find((**it).id());
			m_watches.erase(pos);
		}
	}
}

// timer_history
// Called to update history

void PointServer::timer_history()
{
	// __WRITE_LOCK__
	Poco::ScopedWriteRWLock l(m_lock);

	// store linear history
	for(recs_t::const_iterator it=m_recs.begin(),e=m_recs.end();it!=e;it++)
	{
		Dict *row=(Dict *) it->second;
		his_store(row,"linear");
	}
}

//////////////////////////////////////////////////////////////////////////
// Triggers
//////////////////////////////////////////////////////////////////////////

// engineTrigger
// Call to signal a trigger event

int PointServer::engineTrigger(ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// std::cout << "-- MESSAGE [sys::Engine] trigger channel=" << channel << ", trigger=" << value->trigger << std::endl;
	LOG_DEBUG_HAYSTACKOPS_MSG("trigger channel=%d, trigger=%d",channel,value->trigger);
	// what trigger?
	switch(value->trigger)
	{
		case ENGINE_TRIGGER_IPRESET:
			// ip reset
			triggerIPReset();
			break;
	}
	return 0;
}

// triggerIPReset
// Called when ip reset trigger received

int PointServer::triggerIPReset()
{
	static const char *iface="eth0";	// todo: config

	static const char *address="10.0.0.1";
	static const char *netmask="255.255.255.0";
	static const char *gateway="10.0.0.1";

	static int reset=0;

	// reset only once
	if(reset>0)
	{
		// std::cout << "-- MESSAGE [sys::Haystack] ip reset already" << std::endl;
		LOG_DEBUG_HAYSTACKOPS_MSG("ip reset already");
		return -1;
	}

	// open socket
	int fd=socket(AF_INET,SOCK_DGRAM,0);

	if(fd<0)
	{
		// std::cout << "-- ERROR [sys::Haystack] ip reset failed to open socket" << std::endl;
		LOG_ERR_HAYSTACKOPS_MSG("ip reset failed to open socket");
		return -1;
	}

	// get socket info
	struct ifreq ifr;

	ifr.ifr_addr.sa_family=AF_INET;
	strncpy(ifr.ifr_name,iface,IFNAMSIZ-1);

	int err=ioctl(fd,SIOCGIFHWADDR,&ifr);

	// close socket
	close(fd);

	if(err<0)
	{
		// std::cout << "-- ERROR [sys::Haystack] ip reset failed to get socket info" << std::endl;
		LOG_ERR_HAYSTACKOPS_MSG("ip reset failed to get socket info");
		return -1;
	}

	// construct system command
	char mac[16];
	char cable[64];
	char cmd[200];

	unsigned char *data=(unsigned char *) ifr.ifr_hwaddr.sa_data;

	snprintf(mac,sizeof(mac),"%.2x%.2x%.2x%.2x%.2x%.2x",data[0],data[1],data[2],data[3],data[4],data[5]);
	snprintf(cable,sizeof(cable),"ethernet_%s_cable",mac);

	snprintf(cmd,sizeof(cmd),"connmanctl config %s --ipv4 manual %s %s %s",cable,address,netmask,gateway);

	// std::cout << "-- MESSAGE [sys::Haystack] ip reset " << address << std::endl;
	LOG_DEBUG_HAYSTACKOPS_MSG("ip reset %s",address);

	system(cmd);
	reset=1;

	return 0;
}

//////////////////////////////////////////////////////////////////////////
// Engine IO
//////////////////////////////////////////////////////////////////////////

// engineio_signal_trigger
// Called by engineio to signal event trigger

int engineio_signal_trigger(ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	return PointServer::engineTrigger(channel,value);
}

// engineio_signal_writeack
// Called by engineio to signal write ack

int engineio_signal_writeack(ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	return PointServer::engineWriteAck(channel,value);
}

//////////////////////////////////////////////////////////////////////////
// PointWatch Impl
//////////////////////////////////////////////////////////////////////////

enum { DEFAULT_LEASE_TIME = 5 * 60 }; // 5min in seconds; todo: config

#undef max
#undef min

// Construction

PointWatch::PointWatch(const PointServer &server,const std::string &dis):
	m_server(server),
	m_uuid(boost::lexical_cast<std::string>(boost::uuids::random_generator()())),
	m_dis(dis),
	m_lease(std::numeric_limits<Poco::AtomicCounter::ValueType>::min()),
	m_is_open(false),
	m_values(new Dict)
{
}

// Property access

const std::string PointWatch::id() const
{
    return m_uuid;
}

const std::string PointWatch::dis() const
{
    return m_dis;
}

const int PointWatch::lease() const
{
    return m_lease;
}

void PointWatch::lease(int value)
{
    m_lease = value;
}

// Methods

// sub
// Call to subscribe to watch
#if 0
Grid::auto_ptr_t PointWatch::sub(const refs_t &ids,bool checked)
{
	Poco::ScopedReadRWLock l(m_server.m_lock);
	// copy ids to watch
	//m_ids=ids;

	//for(refs_t::const_iterator id=ids.begin(),e=ids.end();id!=e;id++)
	//{
	//	m_ids.push_back(*id);
	//}

	// TODO: Above commented push_back() is not working. Gives compilation err.
	// Hence we are using insert.
	// TODO: Please check for id duplication
	m_ids.insert(m_ids.end(), ids.begin(), ids.end());

	const PointServer::recs_t &recs=m_server.m_recs;
	std::vector<const Dict *> res;

	// run over ids in watch list
	for(refs_t::iterator id=m_ids.begin();id!=m_ids.end();)
	{
		// check id exists
		PointServer::recs_t::const_iterator rec=recs.find(id->value);

		if(rec==recs.end())
		{
			if(checked)
				throw std::runtime_error("Id not found: "+id->value);

			// remove from watch and point to next ID
            id=m_ids.erase(id);
			continue;
		}

		// std::cout << "-- MESSAGE [sys::Haystack] watching '" << id->value << "'" << std::endl;
		LOG_DEBUG_HAYSTACKOPS_MSG("watching '%s'",id->value.c_str());

		// set initial change value
		const Val &val=rec->second->get("curVal",false);
		m_values->update(id->value,val);

		// add to response
        res.push_back(rec->second);

		// Point to next ID
		++id;
	}

	// reset lease time
	m_lease=DEFAULT_LEASE_TIME;
	m_is_open=true;

	// create result grid
	Grid::auto_ptr_t g=Grid::make(res);

	g->meta().add("watchId",m_uuid);
	g->meta().add("lease",m_lease);

	return g;
}
#endif
Grid::auto_ptr_t PointWatch::sub(const refs_t &ids,bool checked)
{
	Poco::ScopedReadRWLock l(m_server.m_lock);
	std::vector<const Dict *> res;
	for(auto& id : ids) {
		const auto&	rec=m_server.m_recs.find(id.value);
		if(rec == std::end(m_server.m_recs)){
			LOG_DEBUG_HAYSTACKOPS_MSG("Need to push empty row watching '%s'",id.value.c_str());
			if(std::begin(m_server.m_recs) != std::end(m_server.m_recs)){
				auto row = std::begin(m_server.m_recs);
				Dict *ptr = row->second;
				Dict *ptr1 = new Dict();
				for(Dict::const_iterator it = ptr->begin();it!= ptr->end();++it){
					ptr1->add(it->first,new EmptyVal());
				}
				res.push_back(ptr1);
			}
		}
		else {
			LOG_DEBUG_HAYSTACKOPS_MSG("watching '%s'",id.value.c_str());
			const Val &val=rec->second->get("curVal",false);
			m_values->update(id.value,val);
	    res.push_back(rec->second);
		}
	}
	m_ids.insert(m_ids.end(), ids.begin(), ids.end());
	m_lease=DEFAULT_LEASE_TIME;
	m_is_open=true;
	Grid::auto_ptr_t g=Grid::make(res);
	g->meta().add("watchId",m_uuid);
	g->meta().add("lease",m_lease);
	LOG_DEBUG_HAYSTACKOPS_MSG("retruning grid");
	return g;
}

// unsub
// Call to unsubscribe from watch

void PointWatch::unsub(const refs_t &ids)
{
	Poco::ScopedReadRWLock l(m_server.m_lock);
	// run over ids in watch list
	for(refs_t::iterator id=m_ids.begin();id!=m_ids.end();)
	{
		// id in list sent?
		if(std::find(ids.begin(),ids.end(),*id)!=ids.end())
		{
			// std::cout << "-- MESSAGE [sys::Haystack] unwatching '" << id->value << "'" << std::endl;
			LOG_DEBUG_HAYSTACKOPS_MSG("unwatching '%s'",id->value.c_str());

			// remove it from watch and point to next ID
			id=m_ids.erase(id);
		}
		else
		{
			// Point to next ID
			++id;
		}
	}
}

// poll_changes
// Call to return grid of changed values

Grid::auto_ptr_t PointWatch::poll_changes()
{
	// ask for changes
	//return poll_check(false);
	return poll_check(true);
}

// poll_refresh
// Call to return grid of all watched values

Grid::auto_ptr_t PointWatch::poll_refresh()
{
	// ask for everything
	return poll_check(true);
}

// close
// Call to mark watch as closed

void PointWatch::close()
{
	m_is_open=false;
}

// is_open
// Call to check if watch is open

bool PointWatch::is_open() const
{
	return m_is_open;
}

// Local functions

// poll_check
// Called to build grid of chaned values for watch

Grid::auto_ptr_t PointWatch::poll_check(bool refresh)
{
	Poco::ScopedReadRWLock l(m_server.m_lock);

	const PointServer::recs_t &recs=m_server.m_recs;
	boost::ptr_vector<Dict> res;

	// run over ids in watch list
	for(refs_t::iterator id=m_ids.begin(),e=m_ids.end();id!=e;id++)
	{
		// check id still exists
		PointServer::recs_t::const_iterator rec=recs.find(id->value);

		if(rec!=recs.end())
		{
			// test for value changed
			bool changed=false;

			// pass back everything?
			if(refresh) changed=true;
			else
			{
				// has new value changed?
				const Val &val1=rec->second->get("curVal",false);
				const Val &val2=m_values->get(id->value,false);

				if(val2==EmptyVal::DEF || val1!=val2)
				{
					// keep current value
					m_values->update(id->value,val1);
					changed=true;
				}
			}

			// value has changed?
			if(changed)
			{
				// std::cout << "-- MESSAGE [sys::Haystack] watch sending '" << id->value << "'" << std::endl;
				LOG_INFO_HAYSTACKOPS_MSG("watch sending '%s'",id->value.c_str());

				// clone the record
				//Dict::auto_ptr_t row(new Dict);
				//row->add(*rec->second);

				// add to response
		        //res.push_back(row);
				res.push_back(((Dict*)rec->second)->clone());
			}
		}
    }

	// reset lease time
	m_lease=DEFAULT_LEASE_TIME;

	// create result grid
	Grid::auto_ptr_t g=Grid::make(res);

	g->meta().add("watchId",m_uuid);
	g->meta().add("lease",m_lease);

	return g;
}

//////////////////////////////////////////////////////////////////////////
// PointHistory Impl
//////////////////////////////////////////////////////////////////////////

// Construction

PointHistory::PointHistory(const std::string &id)
{
}

// Methods

// Store
// Call to store point history value

void PointHistory::Store(const std::string &curStatus,const Val &curVal,const TimeZone &timeZone/*=TimeZone::DEFAULT*/,bool totalized/*=false*/)
{
	// accumulate total value?
	if(totalized)
	{
// todo: get last entry and add it to curVal
	}

	// store current value
	HisItem item(DateTime::now(timeZone),curVal);
	m_items.push_back(item);

	// pop one if over size limit
	if(m_items.size()>MAX_HISTORY)
		m_items.pop_front();
}

// Fetch
// Call to fetch history items within date range

void PointHistory::Fetch(const DateTimeRange &range,std::vector<HisItem> &his)
{
	// filter out items within range
	const int64_t st=range.start().millis();
	const int64_t nd=range.end().millis();

	for(std::deque<HisItem>::const_iterator it=m_items.begin(),e=m_items.end();it!=e;it++)
	{
		const int64_t tm=it->ts->millis();

		// within date and time range?
		if(tm<=st) continue;
		if(tm>nd) break;

		// add it to response list
		boost::shared_ptr<const DateTime> ts((DateTime *) it->ts->clone().release());
		boost::shared_ptr<const Val> val(it->val->clone().release());

		his.push_back(HisItem(ts,val));
	}
}
