/**
 * HNA is the singleton value used to indicate not available.
 *
 * @see <a href='http://project-haystack.org/doc/TagModel#tagKinds'>Project Haystack</a>
 */

#include "na.hpp"

////////////////////////////////////////////////
// NA
////////////////////////////////////////////////
using namespace haystack;

const Na& Na::NA = Na();


////////////////////////////////////////////////
// to string
////////////////////////////////////////////////

// Encode as "na"
const std::string Na::to_string() const
{
    return "na";
}

////////////////////////////////////////////////
// to zinc
////////////////////////////////////////////////

// Encode as "NA"
const std::string Na::to_zinc() const
{
    return "NA";
}

////////////////////////////////////////////////
// Equal
////////////////////////////////////////////////
bool Na::operator ==(const Na&) const
{
    // all NA are the same
    return true;
}
bool Na::operator==(const Val &other) const
{
    return type() == other.type();
}

////////////////////////////////////////////////
// Cmp
////////////////////////////////////////////////
bool Na::operator > (const Val& other) const
{
    return  type() == other.type() && to_string() > other.to_string();
}
bool Na::operator < (const Val& other) const
{
    return  type() == other.type() && to_string() < other.to_string();
}


Na::auto_ptr_t Na::clone() const
{
    return auto_ptr_t(new Na());
}