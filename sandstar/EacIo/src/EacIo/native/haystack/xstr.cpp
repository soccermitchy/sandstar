/**
 * XStr is an extended string which is a type name and value
 * encoded as a string. It is used as a generic value when an
 * XStr is decoded without any predefined type.
 */

#include <stdio.h>
#include <ctype.h>
#include <sstream>
#include <stdexcept>
#include "xstr.hpp"

// Log
// #include <plog/Log.h>

////////////////////////////////////////////////
// XStr
////////////////////////////////////////////////
using namespace haystack;

XStr::XStr(const std::string type, const std::string val) : value(val)
{
	//LOG_DEBUG << "Params " << type << ":" << val;
	if (!isValidType(type)) 
	{
		throw std::runtime_error("Invalid TYPE: \"" + type + "\"");
	}
	
	xstr_type = type;
	value = val;
	//LOG_DEBUG << "Obj " << xstr_type << ":" << value;
}

bool XStr::isValidType(std::string t)
{
	if (t.empty() || !isupper(t.at(0))) return false;
#if 0
	char[] chars = t.toCharArray();
	for (int i=0; i<chars.length; ++i)
	{
		if (Character.isLetter(chars[i])) continue;
		if (Character.isDigit(chars[i])) continue;
		if (chars[i] == '_') continue;
		return false;
	}
#endif //0
	return true;
}

// MIME type
const std::string XStr::to_string() const
{
    return (xstr_type + "(" + value + ")");
}

////////////////////////////////////////////////
// to zinc
////////////////////////////////////////////////

// Encode as TYPE("VALUE")
const std::string XStr::to_zinc() const
{
    std::stringstream os;
	
	os << (xstr_type + "(\"");

    for (std::string::const_iterator it = value.begin(), end = value.end(); it != end; ++it)
    {
        int c = *it;
        if (c > 127 || c == ')')
        {
            std::stringstream ss;
            ss << "Invalid mime, char='" << (char)c << "'";
            throw std::runtime_error(ss.str().c_str());
        }
        os << (char)c;
    }
    os << "\")";
	
	//LOG_DEBUG << xstr_type << ":" << value;
	//LOG_DEBUG << os.str();
	
    return os.str();
}

////////////////////////////////////////////////
// Equal
////////////////////////////////////////////////
bool XStr::operator ==(const XStr &other) const
{
    return ((xstr_type == other.xstr_type) && (value == other.value));
}

bool XStr::operator==(const Val &other) const
{
    if (type() != other.type())
        return false;
    return static_cast<const XStr&>(other).operator==(*this);
}

bool XStr::operator ==(const std::string &other) const
{
    //return value == other;
	return true;
}

bool XStr::operator < (const Val &other) const
{
    //return type() == other.type() && value < ((Bin&)other).value;
	return true;
}

bool XStr::operator >(const Val &other) const
{
    //return type() == other.type() && value > ((Bin&)other).value;
	return true;
}

XStr::auto_ptr_t XStr::clone() const
{
    return auto_ptr_t(new XStr(*this));
}

//DENKO

void *XStr::decode(std::string type, const std::string &val)
{
    if (type=="Bin") return new Bin(val);

    return new XStr(type, val);
}
/*
Xstr XStr::encode(Object val)
{
    return new HXStr(val.getClass().getSimpleName(), val.toString());
}
*/
