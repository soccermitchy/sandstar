
// file : logger.cpp
// author : Manthan R. Tilva
// date : 14-06-2018
// History : Adding support for filters 14-06-2018

#include "logger.h"

#include <algorithm>

namespace haystack {

std::map<Logger::LoggerFilter, std::string> Logger::m_oFilterName = {
    {LoggerFilter::ALL, "ALL"},
    {LoggerFilter::ENGINE, "ENGINE"},
    {LoggerFilter::LOGIN, "LOGIN"},
    {LoggerFilter::USERS, "USERS"},
    {LoggerFilter::HAYSTACKOPS, "HAYSTACKOPS"},
    {LoggerFilter::POINTWRITE, "POINTWRITE"},
    {LoggerFilter::IPCS, "IPCS"},
    {LoggerFilter::STARTUP, "STARTUP"},
    {LoggerFilter::UNNAMED, "UNNAMED"}};
std::unordered_map<Logger::LoggerFilter, std::unique_ptr<Logger>,
                   Logger::FilterHasher>
    Logger::m_oLoggerMap;
std::vector<std::string> Logger::m_oLevelList = {"NONE",   "EMERG", "ALERT",
                                                 "CRIT",   "ERR",   "WARNING",
                                                 "NOTICE", "INFO",  "DEBUG"};

Logger::LogLevel Logger::m_oLogLevelStatic = {Logger::LogLevel::NONE};
Logger::LoggerType Logger::m_oLogTypeStatic = {Logger::LoggerType::UNKNOWN};
void Logger::enableLogFilter(const LoggerFilter &filter,
                             const LogLevel &level) {
  if (filter != LoggerFilter::ALL) {
    auto &logger = getInstance(filter);
    logger->setLevel(level);
  } else {
    m_oLogLevelStatic = level;
  }
}
Logger::LoggerFilter Logger::filterStringToClass(const std::string &filter) {
  auto ret = std::find_if(
      std::begin(Logger::m_oFilterName), std::end(Logger::m_oFilterName),
      [&filter](const decltype(Logger::m_oFilterName)::value_type &value) {
        return value.second == filter;
      });
  if (ret != std::end(Logger::m_oFilterName))
    return ret->first;
  return LoggerFilter::UNNAMED;
}
Logger::LogLevel Logger::levelStringToClass(const std::string &level) {
  auto pos = std::find(std::begin(Logger::m_oLevelList),
                       std::end(Logger::m_oLevelList), level) -
             std::begin(Logger::m_oLevelList);
  Logger::LogLevel ret = LogLevel::NONE;
  switch (pos) {
  case LogLevel::EMERG:
    ret = LogLevel::EMERG;
    break;
  case LogLevel::ALERT:
    ret = LogLevel::ALERT;
    break;
  case LogLevel::CRIT:
    ret = LogLevel::CRIT;
    break;
  case LogLevel::ERR:
    ret = LogLevel::ERR;
    break;
  case LogLevel::WARNING:
    ret = LogLevel::WARNING;
    break;
  case LogLevel::NOTICE:
    ret = LogLevel::NOTICE;
    break;
  case LogLevel::INFO:
    ret = LogLevel::INFO;
    break;
  case LogLevel::DEBUG:
    ret = LogLevel::DEBUG;
    break;
  }
  return ret;
}
std::unique_ptr<Logger> &Logger::getInstance(const LoggerFilter &filter) {
  auto ret = std::find_if(
      std::begin(Logger::m_oLoggerMap), std::end(Logger::m_oLoggerMap),
      [&filter](const decltype(Logger::m_oLoggerMap)::value_type &value) {
        return value.first == filter;
      });
  if (ret != std::end(Logger::m_oLoggerMap)) {
    return ret->second;
  } else {
    auto logger = std::unique_ptr<Logger>(new Logger());
    logger->setLoggerFilter(filter);
    logger->setLevel(m_oLogLevelStatic);
    Logger::m_oLoggerMap[filter] = std::move(logger);
    return Logger::m_oLoggerMap[filter];
  }
}
void Logger::setLoggerType(const LoggerType &type) {
  m_oLogTypeStatic = type;
  for (auto &logger : Logger::m_oLoggerMap) {
    logger.second->setTypeInternal(type);
  }
}
} // namespace haystack
