
add_subdirectory(src)

add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/etc/database.zinc COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_LIST_DIR}/database.zinc ${CMAKE_BINARY_DIR}/etc/ COMMENT "copying database.zinc")
ADD_CUSTOM_TARGET(copyDatabaseZinc ALL COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/etc/ COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_LIST_DIR}/database.zinc ${CMAKE_BINARY_DIR}/etc/ COMMENT "copying database.zinc")
install(FILES database.zinc DESTINATION etc/)

ADD_CUSTOM_TARGET(copyKitsScode ALL COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/data/ COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_LIST_DIR}/EacIoApp/kits.scode ${CMAKE_BINARY_DIR}/data/ COMMENT "copying kits.scode")
install(FILES EacIoApp/kits.scode DESTINATION data/)

ADD_CUSTOM_TARGET(copyAppSab ALL COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/data/ COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_LIST_DIR}/EacIoApp/app.sab ${CMAKE_BINARY_DIR}/data/ COMMENT "copying app.sab")
install(FILES EacIoApp/app.sab DESTINATION data/)

ADD_CUSTOM_TARGET(copySvmProperties ALL COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/etc/ COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_LIST_DIR}/svm.properties ${CMAKE_BINARY_DIR}/etc/svm$<$<CONFIG:Debug>:d>.properties COMMENT "copying svm.properties")
if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
install(FILES svm.properties DESTINATION etc/ RENAME svmd.properties)
else(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
install(FILES svm.properties DESTINATION etc/ RENAME)
endif(${CMAKE_BUILD_TYPE} STREQUAL "Debug")

ADD_CUSTOM_TARGET(copyPointsCsv ALL COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/etc/ COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_LIST_DIR}/points.csv ${CMAKE_BINARY_DIR}/etc/ COMMENT "copying points.csv")
install(FILES points.csv DESTINATION etc/ )

ADD_CUSTOM_TARGET(copyTablesCsv ALL COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/etc/ COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_LIST_DIR}/tables.csv ${CMAKE_BINARY_DIR}/etc/ COMMENT "copying tables.csv")
install(FILES tables.csv DESTINATION etc/ )

