// poll.h

#ifndef __POLL_H__
#define __POLL_H__

// Definitions

#define MAX_POLLS		64

// Poll item

struct _POLL_ITEM
{
	int nUsed;

	ENGINE_CHANNEL channel;
	ENGINE_VALUE value;
};

typedef struct _POLL_ITEM POLL_ITEM;

// Poll struct

struct _POLL
{
	int nItems;
	int nCount;

	POLL_ITEM *items;
};

typedef struct _POLL POLL;

// Public functions

int poll_init(POLL *poll,int nItems);
int poll_exit(POLL *poll);

int poll_add(POLL *poll,ENGINE_CHANNEL channel);
int poll_remove(POLL *poll,ENGINE_CHANNEL channel);

int poll_watch(POLL *poll,ENGINE_CHANNEL channel,key_t sender);
int poll_notify(POLL *poll,key_t sender);
int poll_update(POLL *poll,CHANNEL *channels,TABLE *tables,WATCH *watch,NOTIFY *notify);

int poll_report(POLL *poll,CHANNEL *channels,TABLE *tables);

#endif // __POLL_H__
