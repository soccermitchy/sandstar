// i2cio.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "global.h"
#include "engine.h"
#include "io.h"
#include "i2cio.h"

// Constants

#define I2CIO_SYSFS		"/dev/i2c"

#define I2CIO_MEASURE	0xF1

#define I2CIO_CRCPOLY	0x131 // P(x) = x^8 + x^5 + x^4 + 1 = 100110001

// Local functions

static unsigned char i2cio_crc(unsigned char cData[],int nLength,unsigned int nPoly);

// Public functions

// i2cio_exists
// Call to check if i2c device exists

int i2cio_exists(I2CIO_DEVICE device,I2CIO_ADDRESS address)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,I2CIO_SYSFS "-%d",device);

	return io_exists(sDevice);
}

// i2cio_get_measurement
// Call to get analog input value

int i2cio_get_measurement(I2CIO_DEVICE device,I2CIO_ADDRESS address,I2CIO_VALUE *value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,I2CIO_SYSFS "-%d",device);

	// open i2c device?
	int hDevice=io_open(sDevice,O_RDWR);
	if(hDevice<0) return -1;

	// set as i2c slave
	if(ioctl(hDevice,I2C_SLAVE,address)<0)
	{
		io_close(hDevice);

		engine_error("io failed to set slave %s:%d",sDevice,address);
		return -1;
	}

	// trigger measurement
	unsigned char cCmd=I2CIO_MEASURE;

	if(write(hDevice,&cCmd,1)!=1)
	{
		io_close(hDevice);

		engine_error("io failed to write cmd byte %s:%d",sDevice,address);
		return -1;
	}

	// read response
	unsigned char cResponse[3];

	if(read(hDevice,cResponse,3)!=3)
	{
		io_close(hDevice);

		engine_error("io failed to read from i2c bus %s:%d",sDevice,address);
		return -1;
	}

	// close device
	io_close(hDevice);

	// perform checksum
	unsigned char cCrc=cResponse[2];

	if(i2cio_crc(cResponse,2,I2CIO_CRCPOLY)!=cCrc)
	{
		engine_error("io checksum failed on i2c bus %s:%d",sDevice,address);
		return -1;
	}

	// set value
	*value=(cResponse[0]<<8)|cResponse[1];

	return 0;
}

// Local functions

// i2cio_crc
// Call to calculate crc8 with given polynomial

static unsigned char i2cio_crc(unsigned char cData[],int nLength,unsigned int nPoly)
{
	unsigned char cCrc=0;
	unsigned char c;

	int b;
	int n;

	for(n=0;n<nLength;n++)
	{
		cCrc^=cData[n];

		for(b=0;b<8;b++)
		{
			c=cCrc;
			cCrc<<=1;

			if(c & 0x80) cCrc^=nPoly;
		}
	}
	return cCrc;
}
