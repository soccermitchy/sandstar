// pwmio.c

// https://www.kernel.org/doc/Documentation/pwm.txt

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <string.h>

#include "global.h"
#include "engine.h"
#include "io.h"
#include "pwmio.h"

// Definitions

#define PWMIO_SYSFS		"/sys/class/pwm"
#define PWMIO_INFFS		"/sys/devices/platform/ocp"

// Local data

static char *sPwmPin[] = {"P9_14", "P9_16", "P8_19", "P8_13",NULL}; //Should end with NULL
static char *g_sPolarity[]={"normal","inversed",NULL};
static char *g_sBoolean[]={"0","1",NULL};

// Public functions

// pwmio_exists
// Call to check if pwm address exists

int pwmio_exists(PWMIO_CHIP chip,PWMIO_CHANNEL channel)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/pwm%d",chip,channel);

	return io_exists(sDevice);
}

// pwmio_export
// Call to export pwm address

int pwmio_export(PWMIO_CHIP chip,PWMIO_CHANNEL channel)
{
	char sDevice[IO_MAXPATH+1];
	char sChannel[IO_MAXBUFFER+1];

	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/export",chip);
	snprintf(sChannel,IO_MAXBUFFER,"%d",channel);

	return io_write(sDevice,sChannel);
}

// pwmio_unexport
// Call to unexport pwm address

int pwmio_unexport(PWMIO_CHIP chip,PWMIO_CHANNEL channel)
{
	char sDevice[IO_MAXPATH+1];
	char sChannel[IO_MAXBUFFER+1];

	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/unexport",chip);
	snprintf(sChannel,IO_MAXBUFFER,"%d",channel);

	return io_write(sDevice,sChannel);
}

// pwmio_get_polarity
// Call to get pwm polarity setting

int pwmio_get_polarity(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_POLARITY *value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/pwm%d/polarity",chip,channel);

	return io_decode(sDevice,g_sPolarity,(int *) value);
}

// pwmio_set_polarity
// Call to set pwm polarity setting

int pwmio_set_polarity(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_POLARITY value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/pwm%d/polarity",chip,channel);

	return io_write(sDevice,g_sPolarity[(int) value]);
}

// pwmio_get_period
// Call to get pwm period setting

int pwmio_get_period(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_PERIOD *value)
{
	char sDevice[IO_MAXPATH+1];
	char sBuffer[IO_MAXBUFFER+1];

	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/pwm%d/period",chip,channel);

	if(io_read(sDevice,sBuffer)>=0)
	{
		*value=(PWMIO_PERIOD) strtol(sBuffer,NULL,10);
		return 0;
	}
	return -1;
}

// pwmio_set_period
// Call to set pwm period setting

int pwmio_set_period(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_PERIOD value)
{
	char sDevice[IO_MAXPATH+1];
	char sBuffer[IO_MAXBUFFER+1];

	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/pwm%d/period",chip,channel);
	snprintf(sBuffer,IO_MAXBUFFER,"%d",value);

	return io_write(sDevice,sBuffer);
}

// pwmio_get_duty
// Call to get pwm duty cycle setting

int pwmio_get_duty(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_DUTY *value)
{
	char sDevice[IO_MAXPATH+1];
	char sBuffer[IO_MAXBUFFER+1];

	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/pwm%d/duty_cycle",chip,channel);

	if(io_read(sDevice,sBuffer)>=0)
	{
		*value=(PWMIO_DUTY) strtol(sBuffer,NULL,10);
		return 0;
	}
	return -1;
}

// pwmio_set_duty
// Call to set pwm duty cycle setting

int pwmio_set_duty(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_DUTY value)
{
	char sDevice[IO_MAXPATH+1];
	char sBuffer[IO_MAXBUFFER+1];

	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/pwm%d/duty_cycle",chip,channel);
	snprintf(sBuffer,IO_MAXBUFFER,"%d",value);

	return io_write(sDevice,sBuffer);
}

// pwmio_get_enable
// Call to get pwm enable setting

int pwmio_get_enable(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_ENABLE *value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/pwm%d/enable",chip,channel);

	return io_decode(sDevice,g_sBoolean,(int *) value);
}

// pwmio_set_enable
// Call to set pwm enable setting

int pwmio_set_enable(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_ENABLE value)
{
	char sDevice[IO_MAXPATH+1];
	snprintf(sDevice,IO_MAXPATH,PWMIO_SYSFS "/pwmchip%d/pwm%d/enable",chip,channel);

	return io_write(sDevice,g_sBoolean[(int) value]);
}

// pwmio_resolve
// Call to resolve chip address

int pwmio_resolve(char *sAddress,PWMIO_CHIP *chip)
{
	int found=0;

	// open platform ocp?
	DIR *d1=opendir(PWMIO_INFFS);
	if(d1==NULL) return -1;

	// read each entry
	struct dirent *e1;

	while(found==0 && (e1=readdir(d1))!=NULL)
	{
		// extension match pwm ss?
		const char *ext1=io_ext(e1->d_name);

		if(strcmp(ext1,"epwmss")==0)
		{
			// path to sub dir
			char dir2[256];
			sprintf(dir2,PWMIO_INFFS "/%s",e1->d_name);

			// open sub dir?
			DIR *d2=opendir(dir2);
			if(d2==NULL) break;

			// read each entry
			struct dirent *e2;

			while(found==0 && (e2=readdir(d2))!=NULL)
			{
				// extension match pwm?
				const char *ext2=io_ext(e2->d_name);

				if(strcmp(ext2,"pwm")==0 || strcmp(ext2,"ecap")==0)
				{
					// get device address
					char addr2[16];
					sscanf(e2->d_name,"%8s",addr2);

					// addresses match?
					if(strcmp(sAddress,addr2)==0)
					{
						// path to pwm dir
						char dir3[256];
						sprintf(dir3,"%s/%s/pwm",dir2,e2->d_name);

						// path to pwm dir
						DIR *d3=opendir(dir3);
						if(d3==NULL) break;

						// read each entry
						struct dirent *e3;

						while(found==0 && (e3=readdir(d3))!=NULL)
						{
							// found chip entry?
							if(strstr(e3->d_name,"pwmchip")==e3->d_name)
							{
								// read chip number
								sscanf(e3->d_name,"pwmchip%d",chip);
								found=1;
							}
						}

						// close pwm dir
						closedir(d3);
					}
				}
			}

			// close sub dir
			closedir(d2);
		}
	}

	// close ocp dir
    closedir(d1);

	return (found>0)?0:-1;
}

// pwmio_config_pwm
// call to configure gpio in pwm mode

int pwmio_config_pwm()
{
	char sDevice[IO_MAXPATH+1];
	int retVal = 0;
	int i;

	for(i=0; (sPwmPin[i] != NULL); i++)
	{
		memset(sDevice, 0, sizeof (sDevice));
		snprintf(sDevice,IO_MAXPATH,"/sys/devices/platform/ocp/ocp:%s_pinmux/state",sPwmPin[i]);
		retVal = io_write(sDevice, "pwm");
		
		if (0!=retVal)
			break;
	}
	
	return retVal;
}
