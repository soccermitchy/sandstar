// pwmio.h

#ifndef __PWMIO_H__
#define __PWMIO_H__

// Polarity type

enum _PWMIO_POLARITY
{
	PWMIO_POLARITY_NORMAL,
	PWMIO_POLARITY_INVERSED
};

typedef enum _PWMIO_POLARITY PWMIO_POLARITY;

// Enable type

enum _PWMIO_ENABLE
{
	PWMIO_ENABLE_DISABLED,
	PWMIO_ENABLE_ENABLED
};

typedef enum _PWMIO_ENABLE PWMIO_ENABLE;

// Typedefs

typedef unsigned int PWMIO_CHIP;
typedef unsigned int PWMIO_CHANNEL;
typedef unsigned int PWMIO_PERIOD;
typedef unsigned int PWMIO_DUTY;

// Public functions

int pwmio_exists(PWMIO_CHIP chip,PWMIO_CHANNEL channel);

int pwmio_export(PWMIO_CHIP chip,PWMIO_CHANNEL channel);
int pwmio_unexport(PWMIO_CHIP chip,PWMIO_CHANNEL channel);

int pwmio_get_polarity(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_POLARITY *value);
int pwmio_set_polarity(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_POLARITY value);

int pwmio_get_period(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_PERIOD *value);
int pwmio_set_period(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_PERIOD value);

int pwmio_get_duty(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_DUTY *value);
int pwmio_set_duty(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_DUTY value);

int pwmio_get_enable(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_ENABLE *value);
int pwmio_set_enable(PWMIO_CHIP chip,PWMIO_CHANNEL channel,PWMIO_ENABLE value);

int pwmio_resolve(char *sAddress,PWMIO_CHIP *chip);

int pwmio_config_pwm();

#endif // __PWMIO_H__
