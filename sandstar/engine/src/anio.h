// anio.h

#ifndef __ANIO_H__
#define __ANIO_H__

// Typedefs

typedef unsigned int ANIO_DEVICE;
typedef unsigned int ANIO_ADDRESS;

typedef double ANIO_VALUE;

// Public functions

int anio_get_value(ANIO_DEVICE device,ANIO_ADDRESS address,ANIO_VALUE *value);

#endif // __ANIO_H__
