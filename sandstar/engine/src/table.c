// table.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include "global.h"
#include "engine.h"
#include "table.h"
#include "csv.h"

// Local functions

static int table_find(TABLE *tables,char *sTag,char *sUnit);
static int table_alloc(TABLE *tables);

// Public functions

// table_init
// Call to initialize table list

int table_init(TABLE *tables,int nItems)
{
	int nSize=nItems*sizeof(TABLE_ITEM);

	tables->nItems=nItems;
	tables->nCount=0;

	tables->items=(TABLE_ITEM *) malloc(nSize);
	memset(tables->items,0,nSize);

	return 0;
}

// table_exit
// Call to release table list

int table_exit(TABLE *tables)
{
	int n;

	// release table values
	for(n=0;n<tables->nItems;n++)
	{
		TABLE_ITEM *item=&tables->items[n];

		// item used?
		if(item->nUsed!=0)
		{
			// release values?
			if(item->values!=NULL)
			{
				free(item->values);
				item->values=NULL;
			}

			item->nValues=0;
			item->nUsed=0;
		}
	}

	// free items?
	if(tables->items!=NULL)
	{
		free(tables->items);
		tables->items=NULL;
	}

	tables->nItems=0;
	tables->nCount=0;

	return 0;
}

// table_load
// Call to load csv table file info

int table_load(TABLE *tables,char *sFile)
{
	// init tables desc
	CSV csv;
	csv_init(&csv);

	// load points file?
	int err=csv_load(&csv,sFile);

	if(err>=0)
	{
		// load table files
		int nRow;

		for(nRow=0;nRow<csv.nRows;nRow++)
		{
			// get table info
			char *sTag=csv_string(&csv,nRow,"tag");
			char *sUnit=csv_string(&csv,nRow,"unit");
			char *sPath=csv_string(&csv,nRow,"path");

			// add the table
			table_add(tables,sTag,sUnit,sPath);
		}
	}

	// release csv desc
	csv_exit(&csv);

	return err;
}

// table_add
// Call to add table to tables list

int table_add(TABLE *tables,char *sTag,char *sUnit,char *sPath)
{
	// already added?
	int n=table_find(tables,sTag,sUnit);
	if(n>=0) return 0;

	// get next free
	n=table_alloc(tables);

	if(n<0)
	{
		engine_error("out of table space");
		return -1;
	}

	// load table?
	int hFile=open(sPath,O_RDONLY);

	if(hFile<0)
	{
		engine_error("table '%s' not found",sPath);
		return -1;
	}

	// get file size
	long nLength=lseek(hFile,0,SEEK_END);
	lseek(hFile,0,SEEK_SET);

	if(nLength<=0)
	{
		close(hFile);

		engine_error("table '%s' is empty",sPath);
		return -1;
	}

	// allocate file memory
	char *pData=(char *) malloc(nLength);

	if(pData==NULL)
	{
		close(hFile);

		engine_error("out of memory loading table '%s'",sPath);
		return -1;
	}

	// read file into memory
	long nSize=read(hFile,pData,nLength);
	close(hFile);

	if(nSize!=nLength)
	{
		free(pData);

		engine_error("table '%s' read error",sPath);
		return -1;
	}

	// enable poll item
	TABLE_ITEM *item=&tables->items[n];

	item->nUsed=1;

	strncpy(item->sTag,sTag,MAX_TABLETAG);
	strncpy(item->sUnit,sUnit,MAX_TABLEUNIT);
	strncpy(item->sPath,sPath,MAX_TABLEPATH);

	// count lines
	int nValues=0;

	for(n=0;n<nLength;n++)
	{
		if(pData[n]=='\n')
			nValues++;
	}

	//
	if(nValues==0)
	{
		free(pData);

		engine_error("table '%s' has no values",sPath);
		return -1;
	}

	// allocate table
	item->nValues=nValues;
	item->values=malloc(sizeof(TABLE_VALUE)*nValues);

	// load table values
	int v;

	for(n=0,v=0;v<nValues && n<nLength;v++)
	{
		item->values[v]=strtod(pData+n,NULL);
		while(n<nLength && pData[n++]!='\n');
	}

	// work out table direction
	TABLE_VALUE first=item->values[0];
	TABLE_VALUE last=item->values[nValues-1];

	item->nDirection=(last>first)?1:-1;

	// release file
	free(pData);

	// add to count
	tables->nCount++;

	return 0;
}

// table_report
// Call to display list of tables

int table_report(TABLE *tables)
{
	// output header
	printf("table   tag              unit    path\n");
	printf("------- ---------------- ------- -------\n");

	// output table items
	int n;

	for(n=0;n<tables->nItems;n++)
	{
		TABLE_ITEM *item=&tables->items[n];

		// item in use?
		if(item->nUsed!=0)
		{
			printf("%-7d %-16.16s %-7.7s %s\n",
				n,
				item->sTag,
				item->sUnit,
				item->sPath);
		}
	}
	printf("\n");

	return 0;
}

// Local functions

// table_find
// Call to find existing table item

static int table_find(TABLE *tables,char *sTag,char *sUnit)
{
	int n;

	for(n=0;n<tables->nItems;n++)
	{
		// item found?
		TABLE_ITEM *item=&tables->items[n];

		if(item->nUsed!=0 &&
			strcmp(item->sTag,sTag)==0 &&
			strcmp(item->sUnit,sUnit)==0)
			return n;
	}
	return -1;
}

// table_alloc
// Call to find first free table item

static int table_alloc(TABLE *tables)
{
	int n;

	for(n=0;n<tables->nItems;n++)
	{
		// item not in use?
		if(tables->items[n].nUsed==0)
			return n;
	}
	return -1;
}
