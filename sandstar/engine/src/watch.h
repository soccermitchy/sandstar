// watch.h

#ifndef __WATCH_H__
#define __WATCH_H__

// Definitions

#define MAX_WATCHES		64

// Watch item

struct _WATCH_ITEM
{
	int nUsed;

	ENGINE_CHANNEL channel;
	key_t sender;
};

typedef struct _WATCH_ITEM WATCH_ITEM;

// Watch struct

struct _WATCH
{
	int nItems;
	int nCount;

	WATCH_ITEM *items;
};

typedef struct _WATCH WATCH;

// Public functions

int watch_init(WATCH *watch,int nItems);
int watch_exit(WATCH *watch);

int watch_add(WATCH *watch,ENGINE_CHANNEL channel,key_t sender);
int watch_remove(WATCH *watch,ENGINE_CHANNEL channel,key_t sender);

int watch_send(key_t sender,ENGINE_CHANNEL channel,ENGINE_VALUE *value);
int watch_update(WATCH *watch,ENGINE_CHANNEL channel,ENGINE_VALUE *value);

int watch_report(WATCH *watch);

#endif // __WATCH_H__
