// anio.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "global.h"
#include "engine.h"
#include "io.h"
#include "anio.h"

// Constants

#define ANIO_SYSFS		"/sys/bus/iio/devices"

// Public functions

// anio_get_value
// Call to get analog input value

int anio_get_value(ANIO_DEVICE device,ANIO_ADDRESS address,ANIO_VALUE *value)
{
	char sDevice[IO_MAXPATH+1];
	char sBuffer[IO_MAXBUFFER+1];
	
	snprintf(sDevice,IO_MAXPATH,ANIO_SYSFS "/iio:device%d/in_voltage%d_raw",device,address);

	if(io_read(sDevice,sBuffer)>=0)
	{
		*value=(ANIO_VALUE) strtol(sBuffer,NULL,10);
		return 0;
	}
	return -1;
}
