1] Copy files 'sandstar.service' to /etc/systemd/system/
2] Reload systemd
	systemctl daemon-reload
3] To Start service:
	systemctl start sandstar.service
4] To Stop service:
	systemctl stop sandstar.service
5] To Restart service:
	systemctl restart sandstar.service
